/**********************************************************
* Exercise1_5.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

public class Exercise1_5 {
	public static void main(String[] args) {
		System.out.println("(9.5 * 4.5 - 2.5 * 3) / (45.5 - 3.5) = " + (9.5 * 4.5 - 2.5 * 3) / (45.5 - 3.5));
	}
}
