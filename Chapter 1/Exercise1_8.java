/**********************************************************
* Exercise1_8.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise1_8 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		double radius = input.nextDouble();
		double perimeter = 2 * radius * Math.PI;
		double area = radius * radius * Math.PI;
		System.out.println("perimeter = " + perimeter);
		System.out.println("area = " + area);
	}
}
