/**********************************************************
* Exercise1_11.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

public class Exercise1_11 {
	public static void main(String[] args) {
		int birth = 7;
		int death = 13;
		int immigrant = 45;
		int currentPopulation = 312032486;
		int year = 365;
		double yearlyBirths = Math.pow(60, 2) * 24 * year / birth;
		double yearlyDeaths = Math.pow(60, 2) * 24 * year / death;
		double yearlyImmigrants = Math.pow(60, 2) * 24 * year / immigrant;
		double yearlyPopulation = yearlyBirths + yearlyImmigrants - yearlyDeaths;

		System.out.println("Current Year Population = " + currentPopulation);
		System.out.println("Next Year's Population = " + currentPopulation + yearlyPopulation);
		System.out.println("Next Two Year's Population = " + currentPopulation + yearlyPopulation * 2);
		System.out.println("Next Three Year's Population = " + currentPopulation + yearlyPopulation * 3);
		System.out.println("Next Four Year's Population = " + currentPopulation + yearlyPopulation * 4);
		System.out.println("Next Five Year's Population = " + currentPopulation + yearlyPopulation * 5);
	}
}
