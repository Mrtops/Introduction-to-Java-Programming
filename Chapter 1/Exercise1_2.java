/**********************************************************
* Exercise1_2.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

public class Exercise1_2 {
	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
			System.out.println("Welcome to Java!");
		}
	}
}
