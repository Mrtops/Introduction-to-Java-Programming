/**********************************************************
* Exercise1_4.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

public class Exercise1_4 {
	public static void main(String[] args) {
		System.out.println("a    a^2    a^3");
		System.out.println("1    1      1");
		System.out.println("2    4      8");
		System.out.println("3    9      27");
		System.out.println("4    16     64");
	}
}
