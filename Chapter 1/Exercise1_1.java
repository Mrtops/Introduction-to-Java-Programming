/**********************************************************
* Exercise1_1.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

public class Exercise1_1 {
	public static void main(String[] args) {
		System.out.println("Welcome to Java!");
		System.out.println("Welcome to Computer Science!");
		System.out.println("Programming is Fun!");
	}
}
