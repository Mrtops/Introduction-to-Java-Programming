/**********************************************************
* Exercise1_6.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

public class Exercise1_6 {
	public static void main(String[] args) {
		System.out.println("1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 = " + (1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9));
	}
}
