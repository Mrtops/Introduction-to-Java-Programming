# Introduction to Java Programming, AP<sup>®</sup> Edition

## Chapter 1

### [Exercise 1-1](/Chapter%201/Exercise1_1.java)

Problem: (_Display three messages_) Write a program that displays <span style="color:green">Welcome to Java</span>, <span style="color:green">Welcome to Computer Science</span>, and <span style="color:green">Programming is fun</span>.

<details><summary>Solution</summary>

``` java
public class Exercise1_1 {
	public static void main(String[] args) {
		System.out.println("Welcome to Java!");
		System.out.println("Welcome to Computer Science!");
		System.out.println("Programming is Fun!");
	}
}
```

</details>

### [Exercise 1-2](/Chapter%201/Exercise1_2.java)

Problem: (_Display five messages_) Write a program that displays <span style="color:green">Welcome to Java</span> five times.

<details><summary>Solution</summary>

``` java
public class Exercise1_2 {
	public static void main(String[] args) {
		for(int i=0; i<5; i++) {
			System.out.println("Welcome to Java!");
		}
	}
}
```

</details>

### [Exercise 1-3](/Chapter%201/Exercise1_3.java)

Problem: (_Display a pattern_) Write a program that displays the following pattern:
```
             J        A       V     V       A
             J       A A       V   V       A A
         J   J      AAAAA       V V       AAAAA
          J J      A     A       V       A     A
```

<details><summary>Solution</summary>

``` java
public class Exercise1_3 {
	public static void main(String[] args) {
				System.out.println("     J        A       V     V       A       ");
				System.out.println("     J       A A       V   V       A A      ");
				System.out.println(" J   J      AAAAA       V V       AAAAA     ");
				System.out.println("  J J      A     A       V       A     A    ");
	}
}
```

</details>

### [Exercise 1-4](/Chapter%201/Exercise1_4.java)

Problem: (_Print a table_) Write a program that displays the following table:
```
		a    a^2    a^3
		1    1      1
		2    4      8
		3    9      27
		4    16     64
```

<details><summary>Solution</summary>

``` java
public class Exercise1_4 {
	public static void main(String[] args) {
		System.out.println("a    a^2    a^3");
		System.out.println("1    1      1");
		System.out.println("2    4      8");
		System.out.println("3    9      27");
		System.out.println("4    16     64");
	}
}
```

</details>

### [Exercise 1-5](/Chapter%201/Exercise1_5.java)

Problem: (_Compute expressions_) Write a program that displays the result of:
``` math
\frac{9.5 * 4.5 - 2.5 * 3}{45.5 - 3.5}
```

<details><summary>Solution</summary>

``` java
public class Exercise1_5 {
	public static void main(String[] args) {
		System.out.println("(9.5 * 4.5 - 2.5 * 3) / (45.5 - 3.5) = " + (9.5 * 4.5 - 2.5 * 3) / (45.5 - 3.5));
	}
}
```

</details>

### [Exercise 1-6](/Chapter%201/Exercise1_6.java)

Problem: (_Summation of a series_) Write a program that displays the result of:
``` math
1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9
```

<details><summary>Solution</summary>

``` java
public class Exercise1_6 {
	public static void main(String[] args) {
		System.out.println("1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 = " + (1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9));
	}
}
```

</details>

### [Exercise 1-7](/Chapter%201/Exercise1_7.java)

Problem: (_Approximate π_) π can be computed using the following formula:
``` math
π = 4 * \bigg(1 - \frac{1}{3} + \frac{1}{5} - \frac{1}{7} + \frac{1}{9} - \frac{1}{11} + ... \bigg)
```
Write a program that displays the result of $`π = 4 * (1 - \frac{1}{3} + \frac{1}{5} - \frac{1}{7} + \frac{1}{9} - \frac{1}{11})`$ and $`π = 4 * (1 - \frac{1}{3} + \frac{1}{5} - \frac{1}{7} + \frac{1}{9} - \frac{1}{11} + \frac{1}{13})`$. Use <span style="color:green">1.0</span> instead of <span style="color:green">1</span> in your program.

<details><summary>Solution</summary>

``` java
public class Exercise1_7 {
	public static void main(String[] args) {
		System.out.println("π ≈ " + 4 * (1.0 - (1.0 / 3.0) + (1.0 / 5.0) - (1.0 / 7.0) + (1.0 / 9.0) - (1.0 / 11.0)));
		System.out.println("π ≈ " + 4 * (1.0 - (1.0 / 3.0) + (1.0 / 5.0) - (1.0 / 7.0) + (1.0 / 9.0) - (1.0 / 11.0) + (1.0 / 13.0)));
	}
}
```

</details>

### [Exercise 1-8](/Chapter%201/Exercise1_8.java)

Problem: (_Area and perimeter of a circle_) Write a program that displays the area and perimeter of a circle that has a radius of 5.5 using the following formula:
``` math
\begin{gathered}
perimeter = 2 * radius * π \\
area = radius * radius * π
\end{gathered}
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise1_8 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		double radius = input.nextDouble();
		double perimeter = 2 * radius * Math.PI;
		double area = radius * radius * Math.PI;
		System.out.println("perimeter = " + perimeter);
		System.out.println("area = " + area);
	}
}
```

</details>

### [Exercise 1-9](/Chapter%201/Exercise1_9.java)

Problem: (_Area and perimeter of a rectangle_) Writ a program that displays the area and perimeter of a rectangle with the width of <span style="color:green">4.5</span> and a height of <span style="color:green">7.9</span> using the following formula:
``` math
area = width * height
```

<details><summary>Solution</summary>

``` java
public class Exercise1_9 {
	public static void main(String[] args) {
		double area;
		double perimeter;
		area = 4.5 * 7.9;
		perimeter = ((4.5 * 2) + (7.9 * 2));
		System.out.println("area = " + area);
		System.out.println("perimeter = " + perimeter);
	}
}
```

</details>

### [Exercise 1-10](/Chapter%201/Exercise1_10.java)

Problem: (_Average speed in miles_) Assume a runner runs <span style="color:green">14</span> kilometers in <span style="color:green">45</span> minutes and <span style="color:green">30</span> seconds. Write a program that displays the average speed in miles per hour. (Note that <span style="color:green">1</span> mile is <span style="color:green">1.6</span> kilometers.)

<details><summary>Solution</summary>

``` java
public class Exercise1_10 {
	public static void main(String[] args) {
		System.out.println("Average speed of runner in mph ≈ " + (14.0 / 1.6) / (((45.0 * 60.0) + 30.0) / 3600));
	}
}
```

</details>

### [Exercise 1-11](/Chapter%201/Exercise1_11.java)

Problem: (_Population Projection_) The U.S. Census Bureau projects population based on the following assumptions:

*   One birth every 7 seconds
*   One death every 13 seconds
*   One new immigrant every 45 seconds

Write a program to display the population for each of the next five years. Assume the current population is 312,032,486 and one year has 365 days. _Hint:_ In Java, if two integers perform division, the result is an integer. The fractional part is truncated. For example, <span style="color:green">5</span> / <span style="color:green">4</span> is <span style="color:green">1</span> (not <span style="color:green">1.25</span>) and <span style="color:green">10</span> / <span style="color:green">4</span> is <span style="color:green">2</span> (not <span style="color:green">2.5</span>). To get an accurate result with the fractional part, one of the values involved in the division must be a number with a decimal point. For example, <span style="color:green">5.0</span> / <span style="color:green">4</span> is <span style="color:green">1.25</span> and <span style="color:green">10</span> / <span style="color:green">4.0</span> is <span style="color:green">2.5</span>.

<details><summary>Solution</summary>

``` java
public class Exercise1_11 {
	public static void main(String[] args) {

		int birth = 7;
		int death = 13;
		int immigrant = 45;
		int currentPopulation = 312032486;
		int year = 365;
		double yearlyBirths = Math.pow(60, 2) * 24 * year / birth;
		double yearlyDeaths = Math.pow(60, 2) * 24 * year / death;
		double yearlyImmigrants = Math.pow(60, 2) * 24 * year / immigrant;
		double yearlyPopulation = yearlyBirths + yearlyImmigrants - yearlyDeaths;

		System.out.println("Current Year Population = " + currentPopulation);
		System.out.println("Next Year's Population = " + currentPopulation + yearlyPopulation);
		System.out.println("Next Two Year's Population = " + currentPopulation + yearlyPopulation * 2);
		System.out.println("Next Three Year's Population = " + currentPopulation + yearlyPopulation * 3);
		System.out.println("Next Four Year's Population = " + currentPopulation + yearlyPopulation * 4);
		System.out.println("Next Five Year's Population = " + currentPopulation + yearlyPopulation * 5);
	}
}
```

</details>

### [Exercise 1-12](/Chapter%201/Exercise1_12.java)

Problem: (_Average speed in kilometers_) Assume a runner runs <span style="color:green">24</span> miles in <span style="color:green">1</span> hour, <span style="color:green">40</span> minutes, and <span style="color:green">35</span> seconds. Write a program that displays the average speed in kilometers per hour. (Note that <span style="color:green">1</span> mile is <span style="color:green">1.6</span> kilometers.)

<details><summary>Solution</summary>

``` java
 public class Exercise1_12 {
	 public static void main(String[] args) {
		 double kilometers = 24 * 1.6;
		 double hours = 1 + (40.0 * 60.0 + 35.0) / 3600;
		 double kph = kilometers / hours;
		 System.out.println("24 miles run in 1 hour, 40 minutes, and 35 seconds is = " + kph + " kph");
	 }
 }
```

</details>

### [Exercise 1-13](/Chapter%201/Exercise1_13.java)

Problem: (_Algebra: solve 2 x 2 linear equations_) You can use Cramer's rule to solve the following 2 x 2 system of linear equasion provided that ad - bc is not 0:
``` math
\begin{aligned}
		ax + by = e \\
		cx + dy = f
\end{aligned}
\kern1em
x = \frac{ed - bf}{ad - bc}
\kern1em
y = \frac{af - ec}{ad - bc}
```
Write a program that solves the following equasion and displays the value for _x_ and _y_: (Hint: replace the symbols in the formula with numbers to compute x and y. This exercise can be done in Chapter 1 without using materials in later chapters.)
``` math
\begin{gathered}
	 3.4x + 50.2y = 44.5 \\
	 2.1x + .55y = 5.9
\end{gathered}
```

 <details><summary>Solution</summary>

 ``` java
 public class Exercise1_13 {
	 public static void main(String[] args) {
			System.out.println("Solving for x and y using Cramer's rule with the following equations:");
			System.out.println("3.4x + 50.2y = 44.5");
			System.out.println("2.1x + .55y = 5.9");
			System.out.println("x = " + (44.5 * .55 - 50.2 * 5.9) / (3.4 * .55 - 50.2 * 2.1));
			System.out.println("y = " + (3.4 * 5.9 - 44.5 * 2.1) / (3.4 * .55 - 50.2 * 2.1));
	 }
 }
 ```

 </details>
