/**********************************************************
* Exercise1_9.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

public class Exercise1_9 {
	public static void main(String[] args) {
		double area;
		double perimeter;
		area = 4.5 * 7.9;
		perimeter = ((4.5 * 2) + (7.9 * 2));
		System.out.println("area = " + area);
		System.out.println("perimeter = " + perimeter);
	}
}
