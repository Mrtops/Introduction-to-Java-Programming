/**********************************************************
* Exercise2_18.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

public class Exercise2_18 {
	public static void main(String[] args) {
		float a = 1;
		float b = 2;

		// Create table
		System.out.println("a 		b 		pow(a, b)");
		System.out.println((int) a + " 		" + (int) b + " 		" + (int) Math.pow(a++, b++));
		System.out.println((int) a + " 		" + (int) b + " 		" + (int) Math.pow(a++, b++));
		System.out.println((int) a + " 		" + (int) b + " 		" + (int) Math.pow(a++, b++));
		System.out.println((int) a + " 		" + (int) b + " 		" + (int) Math.pow(a++, b++));
		System.out.println((int) a + " 		" + (int) b + " 		" + (int) Math.pow(a, b));
	}
}
