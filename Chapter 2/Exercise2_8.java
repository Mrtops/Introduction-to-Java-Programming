/**********************************************************
* Exercise2_8.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_8 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get offset
		System.out.print("Enter the timezone offset to GMT: ");
		int offset = input.nextInt();

		// Get current time in GMT offset
		long totalMilliseconds = System.currentTimeMillis();
		long totalSeconds = totalMilliseconds / 1000;
		long currentSecond = totalSeconds % 60;
		long totalMinutes = totalSeconds / 60;
		long currentMinute = totalMinutes % 60;
		long totalHours = totalMinutes / 60;
		long currentHour = (totalHours % 24) + offset;

		// Print current time from offset
		System.out.println("The current time is " + currentHour + ":" + currentMinute + ":" + currentSecond);
	}
}
