/**********************************************************
* Exercise2_7.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_7 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter the number of minutes: ");
		int minutes = input.nextInt();
		int hours = minutes / 60;
		int days = hours / 24;
		int years = days / 365;
		int remainingDays = days % 365;
		System.out.println(minutes + " is approximately " + years + " years and " + remainingDays + " days");
	}
}
