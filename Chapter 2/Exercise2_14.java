/**********************************************************
* Exercise2_14.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_14 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter weight in pounds: ");
		double pounds = input.nextDouble();
		System.out.print("Enter height in inches: ");
		double inches = input.nextDouble();
		double kilograms = pounds * 0.45359237;
		double meters = inches * 0.0254;
		double BMI = kilograms / Math.pow(meters, 2);
		System.out.println("BMI is " + BMI);
	}
}
