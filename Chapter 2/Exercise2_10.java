/**********************************************************
* Exercise2_10.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_10 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get amount of water in kilograms
		System.out.print("Enter the amount of water in kilograms: ");
		double water = input.nextDouble();

		// Get inital temperature
		System.out.print("Enter inital temperature: ");
		double initalTemperature = input.nextDouble();

		// Get final temperature
		System.out.print("Enter the final temperature: ");
		double finalTemperature = input.nextDouble();

		// Calculate the energy needed
		final double energyNeeded = water * (finalTemperature - initalTemperature) * 4184;
		System.out.println("The energy needed is " + energyNeeded);
	}
}
