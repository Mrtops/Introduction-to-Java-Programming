/**********************************************************
* Exercise2_17.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_17 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Temperature input & validation
		System.out.print("Enter the temperature in Fahrenheit between -58˚F and 41˚F: ");
		double temperature = input.nextDouble();
		if (temperature <= -58.0 || temperature >= 41.0) {
			System.out.println("Not a vaild temperature!");
			System.exit(1);
		}

		// Wind speed input & validation
		System.out.print("Enter wind speed (≥ 2) in miles per hour: ");
		double windSpeed = input.nextDouble();
		if (windSpeed >= 2) {
			System.out.println("Not a vaild wind speed!");
			System.exit(1);
		}

		double windSpeedComp = Math.pow(windSpeed, 0.16);
		final double windChillIndex = 35.74 + (0.6215 * temperature) - (35.75 * windSpeedComp) + (0.4275 * temperature * windSpeedComp);
		System.out.println("The wind chill index is " + windChillIndex);
	}
}
