/**********************************************************
* Exercise2_16.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_16 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter the length of the side: ");
		double side = input.nextDouble();
		final double areaHex = ((3 * Math.pow(3, 0.5)) / 2) * Math.pow(side, 2);
		System.out.println("The area of the hexagon is " + areaHex);
	}
}
