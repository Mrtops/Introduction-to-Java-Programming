/**********************************************************
* Exercise2_1.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_1 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Input a Celsius value to be converted to Fahrenheit: ");
		double celsius = input.nextDouble();
		double fahrenheit = (9.0 / 5) * celsius + 32;
		System.out.println(celsius + " Celsius is " + fahrenheit + " Fahrenheit");
	}
}
