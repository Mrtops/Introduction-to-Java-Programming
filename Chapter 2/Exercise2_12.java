/**********************************************************
* Exercise2_12.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_12 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get speed and acceleration
		System.out.print("Enter speed and acceleration: ");
		double speed = input.nextDouble();
		double acceleration = input.nextDouble();

		// Calculate minimum runway length
		final double length = Math.pow(speed, 2) / (2 * acceleration);
		System.out.println("The minimum runway length for this airplane is " + length);
	}
}
