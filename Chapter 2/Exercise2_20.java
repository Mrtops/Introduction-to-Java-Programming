/**********************************************************
* Exercise2_20.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_20 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get balance and interest rate
		System.out.print("Enter balance and interest rate (e.g., 3 for 3%): ");
		double balance = input.nextDouble();
		double interestRate = input.nextDouble();

		// Calculate and display interest
		final double interest = balance * (interestRate / 1200);
		System.out.println("The interest is " + interest);
	}
}
