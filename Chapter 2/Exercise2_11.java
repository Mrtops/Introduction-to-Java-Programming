/**********************************************************
* Exercise2_11.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_11 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get number of years
		System.out.print("Enter the number of years: ");
		double years = input.nextDouble();

		// Calculate and print current population
		double currentPopulation = 312032486 + 2780096.703 * years;
		System.out.println("The population in " + (int)years + " years is " + (int)currentPopulation);
	}
}
