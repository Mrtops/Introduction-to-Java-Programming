/**********************************************************
* Exercise2_6.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_6 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter a whole number between 0 and 1000: ");
		int number = input.nextInt();
		int digitOne = number % 10;
		number /= 10;
		int digitTwo = number % 10;
		number /= 10;
		int digitThree = number % 10;
		int sumOfDigits = digitOne + digitTwo + digitThree;
		System.out.println("The sum of the digits is " + sumOfDigits);
	}
}
