/**********************************************************
* Exercise2_9.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_9 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get v₀, v₁, and t
		System.out.print("Enter v₀, v₁ and t: ");
		double v0 = input.nextDouble();
		double v1 = input.nextDouble();
		double t = input.nextDouble();

		// Calculate average acceleration
		final double a = (v1 - v0) / t;
		System.out.println("The average acceleration is " + a);
	}
}
