/**********************************************************
* Exercise2_2.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_2 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter the radius and length of a cylinder: ");
		double radius = input.nextDouble();
		double length = input.nextDouble();
		double area = radius  * radius * Math.PI;
		double volume = area * length;
		System.out.println("The area is " + area);
		System.out.println("The volume is " + volume);
	}
}
