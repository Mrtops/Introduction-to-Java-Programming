/**********************************************************
* Exercise2_21.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_21 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get invesment, annual interest rate, and years
		System.out.print("Enter investment amount: ");
		double investmentAmount = input.nextDouble();
		System.out.print("Enter annual interest rate in percentage: ");
		double annualInterestRate = input.nextDouble();
		double monthlyInterestRate = annualInterestRate / 1200;
		System.out.print("Enter number of years: ");
		double numberOfYears = input.nextDouble();

		// Calculate and return future investment value
		final double futureInvestmentValue = investmentAmount * Math.pow(1 + monthlyInterestRate, numberOfYears * 12);
		System.out.printf("Accumulated value is $%.2f%n", futureInvestmentValue);
	}
}
