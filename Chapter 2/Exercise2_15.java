/**********************************************************
* Exercise2_15.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_15 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter x₁ and y₁: ");
		double x1 = input.nextDouble();
		double y1 = input.nextDouble();
		System.out.print("Enter x₂ and y₂: ");
		double x2 = input.nextDouble();
		double y2 = input.nextDouble();
		double xVal = Math.pow(x2 - x1, 2);
		double yVal = Math.pow(y2 - y1, 2);
		double distance = Math.pow(xVal + yVal, 0.5);
		System.out.println("The distance between the two points is " + distance);
	}
}
