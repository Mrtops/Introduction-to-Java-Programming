/**********************************************************
* Exercise2_13.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise2_13 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get monthly saving amount
		System.out.print("Enter the monthly saving amount: ");
		double monthlyDeposit = input.nextDouble();

		// Calculate value after 6 months
		final double interestRate = 1 + 0.00417;
		final double monthOne = monthlyDeposit * interestRate;
		final double monthTwo = (monthlyDeposit + monthOne) * interestRate;
		final double monthThree = (monthlyDeposit + monthTwo) * interestRate;
		final double monthFour = (monthlyDeposit + monthThree) * interestRate;
		final double monthFive = (monthlyDeposit + monthFour) * interestRate;
		final double monthSix = (monthlyDeposit + monthFive) * interestRate;

		// Return value
		System.out.println("After the sixth month, the account value is $" + monthSix);
	}
}
