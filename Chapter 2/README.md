# Introduction to Java Programming, AP<sup>®</sup> Edition

## Chapter 2

### [Exercise 2-1](/Chapter%202/Exercise2_1.java)

 Problem: (_Convert Celsius to Fahrenheit_) Write a program that reads a Celsius degree in a <span style="color:green">double</span> value from the console, then converts in to Fahrenheit and displays the result the formula for the conversion is as follows:
 ``` math
 fahrenheit = \frac{9}{5} * celsius + 32
 ```
 _Hint_: In Java, <span style="color:green">9 / 5</span> is <span style="color:green">1</span>, but <span style="color:green">9.0 / 5</span> is <span style="color:green">1.8</span>.

 Here is a sample run:
 ```
 Enter a degree in Celsius: 43.5 [⏎Enter]
 43.5 Celsius is 110.3 Fahrenheit
 ```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_1 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Input a Celsius value to be converted to Fahrenheit: ");
		double celsius = input.nextDouble();
		double fahrenheit = (9.0 / 5) * celsius + 32;
		System.out.println(celsius + " Celsius is " + fahrenheit + " Fahrenheit");
	}
}
```

</details>

### [Exercise 2-2](/Chapter%202/Exercise2_2.java)

Problem: (_Compute the volume of a cylinder_) Write a program that reads in the radius and length of a cylinder and computes the area and volume using the following formulas:
``` math
\begin{gathered}
area = radius * radius * π \\
volume = area * length
\end{gathered}
```
Here is a sample run:
```
Enter the radius and length of a cylinder: 5.5 12 [⏎Enter]
The area is 95.0331
The volume is 1140.4
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_2 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter the radius and length of a cylinder: ");
		double radius = input.nextDouble();
		double length = input.nextDouble();
		double area = radius  * radius * Math.PI;
		double volume = area * length;
		System.out.println("The area is " + area);
		System.out.println("The volume is " + volume);
	}
}
```

</details>

### [Exercise 2-3](/Chapter%202/Exercise2_3.java)

Problem: (_Convert feet into meters_) Write a program that reads a number in feet, converts it to meters, and displays the result. One foot is <span style="color:green">0.305</span> meters. Here is a sample run:
```
Enter a value for feet: 16.5 [⏎Enter]
16.5 feet is 5.0325 meters
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_3 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter a value for feet: ");
		double feet = input.nextDouble();
		double meters = feet * 0.305;
		System.out.println(feet + " feet is " + meters + " meters");
	}
}
```

</details>

### [Exercise 2-4](/Chapter%202/Exercise2_4.java)

Problem: (_Convert pounds into kilograms_) Write a program that converts pounds into kilograms. The program prompts the user to enter a number in pounds, converts it to kilograms, and displays the result. One pound is <span style="color:green">0.454</span> kilograms. Here is a sample run:
```
Enter a number in pounds: 55.5 [⏎Enter]
55.5 pounds is 25.197 kilograms
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_4 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter a number in pounds: ");
		double pounds = input.nextDouble();
		double kilograms = pounds * 0.454;
		System.out.println(pounds + " pounds is " + kilograms + " kilograms");
	}
}
```

</details>

### [Exercise 2-5](/Chapter%202/Exercise2_5.java)

Problem: (_Financial application: calculate tips_) Write a program that reads the subtotal and gratuity rate, then computes the gratuity and total. For example, is the user enters <span style="color:green">10</span> for subtotal and <span style="color:green">15%</span> for gratuity rate, the program displays <span style="color:green">\$1.5</span> as gratuity and <span style="color:green">\$11.5</span> as total. Here is a sample run:
```
Enter the subtotal and gratuity rate: 10 15 [⏎Enter]
The gratuity is $1.5 and total is $11.5
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_5 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter the subtotal and a gratuity rate: ");
		double subtotal = input.nextDouble();
		double gratuityRate = input.nextDouble() / 100.0;
		double gratuity = subtotal * gratuityRate;
		double total = subtotal + gratuity;
		System.out.println("The gratuity is " + gratuity + " and total is " + total);
	}
}
```

</details>

### [Exercise 2-6](/Chapter%202/Exercise2_6.java)

Problem: (_Sum the digits in an integer_) Write a program that reads an integer between <span style="color:green">0</span> and <span style="color:green">1000</span> and adds all the digits in the integer. For example, if an integer is <span style="color:green">932</span>, the sum of al its digits is <span style="color:green">14</span>.

_Hint_: Use the <span style="color:green">%</span> operator to extract digits, and use the <span style="color:green">/</span> operator to remove the extracted digit. For instance, <span style="color:green">932 % 10 = 2</span> and <span style="color:green">932 / 10 = 93</span>.

Here is a sample run:
```
Enter a number of digits between 0 and 1000: 999 [⏎Enter]
The sum of the digits is 27
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_6 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter a whole number between 0 and 1000: ");
		int number = input.nextInt();
		int digitOne = number % 10;
		number /= 10;
		int digitTwo = number % 10;
		number /= 10;
		int digitThree = number % 10;
		int sumOfDigits = digitOne + digitTwo + digitThree;
		System.out.println("The sum of the digits is " + sumOfDigits);
	}
}
```

</details>

### [Exercise 2-7](/Chapter%202/Exercise2_7.java)

Problem: (_Find the number of years_) Write a program that prompts the user to enter the minutes (eg., 1 billion), and displays the number of years and days for the minutes. For simplcity, assume a year has <span style="color:green">365</span> days. Here is a sample run:
```
Enter the number of minutes: 1000000000 [⏎Enter]
1000000000 minutes is approximately 1902 years and 214 days
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_7 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter the number of minutes: ");
		int minutes = input.nextInt();
		int hours = minutes / 60;
		int days = hours / 24;
		int years = days / 365;
		int remainingDays = days % 365;
		System.out.println(minutes + " is approximately " + years + " years and " + remainingDays + " days");
	}
}
```

</details>

### [Exercise 2-8](/Chapter%202/Exercise2_8.java)

Problem: (_Current Time_) Listing 2.7, ShowCurrentTime.java, gives a program that displays the current time in GMT. Revise the program so that it prompts the user to enter the time zone offset to GMT and displays the time in the specified time zone. Here is a sample run:
```
Enter the time zone offset to GMT: 5 [⏎Enter]
The current time is 4:50:34
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_8 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get offset
		System.out.print("Enter the timezone offset to GMT: ");
		int offset = input.nextInt();

		// Get current time in GMT offset
		long totalMilliseconds = System.currentTimeMillis();
		long totalSeconds = totalMilliseconds / 1000;
		long currentSecond = totalSeconds % 60;
		long totalMinutes = totalSeconds / 60;
		long currentMinute = totalMinutes % 60;
		long totalHours = totalMinutes / 60;
		long currentHour = (totalHours % 24) + offset;

		// Print current time from offset
		System.out.println("The current time is " + currentHour + ":" + currentMinute + ":" + currentSecond);
	}
}
```

</details>

### [Exercise 2-9](/Chapter%202/Exercise2_9.java)

Problem: (_Physics: acceleration_) Average acceleration is defined as the change of velocity divided by the time taken to make the change as shown in the following formula:
``` math
a = \frac{v_1 - v_0}{t}
```
Write a program that prompts the user to enter the starting velocity $`v_0`$ in meters/second, the ending velocity $`v_1`$ in meters/second, and the time span $`t`$ in seconds, and displays the average acceleration. Here is a sample run:
```
Enter v₀, v₁ and t: 5.5 50.9 4.5 [⏎Enter]
The average acceleration is 10.0889
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_9 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get v₀, v₁, and t
		System.out.print("Enter v₀, v₁ and t: ");
		double v0 = input.nextDouble();
		double v1 = input.nextDouble();
		double t = input.nextDouble();

		// Calculate average acceleration
		final double a = (v1 - v0) / t;
		System.out.println("The average acceleration is " + a);
	}
}
```

</details>

### [Exercise 2-10](/Chapter%202/Exercise2_10.java)

Problem: (_Science: calculating energy_) Write a program that calculates the energy needed to heat water from an inital temperature to a final temperature. Your program should prompt the user to enter the amount of water in kilograms and the inital and final temperatures of water. The formula to compute the energy is
``` math
Q = M * (finalTemperature - initalTemperature) * 4184
```
where <span style="color:green">M</span> is the weight of water in kilograms, temperatures are in degrees Celsius, and energy <span style="color:green">Q</span> is measured in joules. Here is a sample run:
```
Enter the amount of water in kilograms: 55.5 [⏎Enter]
Enter the inital temperature: 3.5 [⏎Enter]
Enter the final temperature: 10.5 [⏎Enter]
The energy needed is 1625484.0
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_10 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get amount of water in kilograms
		System.out.print("Enter the amount of water in kilograms: ");
		double water = input.nextDouble();

		// Get inital temperature
		System.out.print("Enter inital temperature: ");
		double initalTemperature = input.nextDouble();

		// Get final temperature
		System.out.print("Enter the final temperature: ");
		double finalTemperature = input.nextDouble();

		// Calculate the energy needed
		final double energyNeeded = water * (finalTemperature - initalTemperature) * 4184;
		System.out.println("The energy needed is " + energyNeeded);
	}
}
```

</details>

### [Exercise 2-11](/Chapter%202/Exercise2_11.java)

Problem: (_Population projection_) Rewrite Programming Exercise 1.11 to prompt the user to enter the number of years and displays the population after the number of years. Use the hint in Programming Exercise 1.11 for this program. The population should be cast to an integer. Here is a sample run of the program:
```
Enter the number of years: 5 [⏎Enter]
The population in 5 years is 325932969
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_11 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get number of years
		System.out.print("Enter the number of years: ");
		double years = input.nextDouble();

		// Calculate and print current population
		double currentPopulation = 312032486 + 2780096.703 * years;
		System.out.println("The population in " + (int)years + " years is " + (int)currentPopulation);
	}
}
```

</details>

### [Exercise 2-12](/Chapter%202/Exercise2_12.java)

Problem: (_Physics: finding runway length_) Given an airplane's acceleration $`a`$ and take-off speed $`v`$, you can compute the minimum runway length needed for an airplane to take off using the following formula:
``` math
length = \frac{v^2}{2a}
```
Write a program that prompts the user to enter $`v`$ in meters/second (m/s) and the acceleration $`a`$ in meters/second squared (m/s²), and displays the minimum runway length. Here is a sample run:
```
Enter speed and acceleration: 60 3.5 [⏎Enter]
The minimum runway length for this airplane is 514.286
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_12 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get speed and acceleration
		System.out.print("Enter speed and acceleration: ");
		double speed = input.nextDouble();
		double acceleration = input.nextDouble();

		// Calculate minimum runway length
		final double length = Math.pow(speed, 2) / (2 * acceleration);
		System.out.println("The minimum runway length for this airplane is " + length);
	}
}
```

</details>

### [Exercise 2-13](/Chapter%202/Exercise2_13.java)

Problem: (_Financial application: compound value_) Suppose you save <span style="color:green">$100</span> each _each_ month into a savings account with the annual intrest rate 5%. Thus, the monthly intrest rate is 0.05/12 = 0.00417. After the first month, the value in the account becomes
``` math
100 * (1 + 0.00417) = 100.417
```
After the second month, the value in the account becomes
``` math
(100 + 100.417) * (1 + 0.00417) = 201.25
```
After the third month, the value in the account becomes
``` math
(100 + 201.225) * (1 + 0.00417) = 302.507
```
and so on.

Write a program that prompts the user to enter a monthly saving amount and displays the account value after the sixth month. (In Exercise 5.30, you will use a loop to simplify the code and display the account value for any month.)
```
Enter the monthly saving amount: 100 [⏎Enter]
After the sixth month, the account value is $608.18
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_13 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get monthly saving amount
		System.out.print("Enter the monthly saving amount: ");
		double monthlyDeposit = input.nextDouble();

		// Calculate value after 6 months
		final double interestRate = 1 + 0.00417;
		final double monthOne = monthlyDeposit * interestRate;
		final double monthTwo = (monthlyDeposit + monthOne) * interestRate;
		final double monthThree = (monthlyDeposit + monthTwo) * interestRate;
		final double monthFour = (monthlyDeposit + monthThree) * interestRate;
		final double monthFive = (monthlyDeposit + monthFour) * interestRate;
		final double monthSix = (monthlyDeposit + monthFive) * interestRate;

		// Return value
		System.out.println("After the sixth month, the account value is $" + monthSix);
	}
}
```

</details>

### [Exercise 2-14](/Chapter%202/Exercise2_14.java)

Problem: (_Health application: computing BMI_) Body Mass Index (BMI) is a measure of health on weight. It can be calculated by taking your weight in kilograms and dividing by the square of your height in meters. Write a program that prompts the user to enter a weight in pounds and height in inches and dispays the BMI. Note that one pound is <span style="color:green">0.45359237</span> kilograms and one inch is <span style="color:green">0.0254</span> meters. Here is a sample run:
```
Enter weight in pounds: 95.5 [⏎Enter]
Enter height in inches: 50 [⏎Enter]
BMI is 26.8573
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_14 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter weight in pounds: ");
		double pounds = input.nextDouble();
		System.out.print("Enter height in inches: ");
		double inches = input.nextDouble();
		double kilograms = pounds * 0.45359237;
		double meters = inches * 0.0254;
		double BMI = kilograms / Math.pow(meters, 2);
		System.out.println("BMI is " + BMI);
	}
}
```

</details>

### [Exercise 2-15](/Chapter%202/Exercise2_15.java)

Problem: (_Geometry: distance of two points_) Write a program that prompts the user to enter two points <span style="color:green">$`(x_1, y_1)`$</span> and <span style="color:green">$`(x_2, y_2)`$</span> and displays their distance. The formula for computing the distance is $`\sqrt{(x_2 - x_1)^2 + (y_2 - y_1)^2}`$. Note that you can use <span style="color:green">Math.pow(a, 0.5)</span> to compute $`\sqrt{a}`$. Here is a sample run:
```
Enter x₁ and y₁: 1.5 -3.4 [⏎Enter]
Enter x₂ and y₂: 4 5 [⏎Enter]
The distance between the two points is 8.764131445842194
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_15 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter x₁ and y₁: ");
		double x1 = input.nextDouble();
		double y1 = input.nextDouble();
		System.out.print("Enter x₂ and y₂: ");
		double x2 = input.nextDouble();
		double y2 = input.nextDouble();
		double xVal = Math.pow(x2 - x1, 2);
		double yVal = Math.pow(y2 - y1, 2);
		double distance = Math.pow(xVal + yVal, 0.5);
		System.out.println("The distance between the two points is " + distance);
	}
}
```

</details>

### [Exercise 2-16](/Chapter%202/Exercise2_16.java)

Problem: (_Gemoetry: area of a hexagon_) Write a program that prompts the user to enter the side of a hexagon and displays its area. The formula for computing the area of a hexagon is
``` math
Area = \frac{3\sqrt{3}}{2}s^2
```
where $`s`$ is the length of a side. Here is a sample run:
```
Enter the length of the side: 5.5 [⏎Enter]
The area of the hexagon is 78.5918
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_16 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter the length of the side: ");
		double side = input.nextDouble();
		final double areaHex = ((3 * Math.pow(3, 0.5)) / 2) * Math.pow(side, 2);
		System.out.println("The area of the hexagon is " + areaHex);
	}
}
```

</details>

### [Exercise 2-17](/Chapter%202/Exercise2_17.java)

Problem: (_Science: wind-chill temperature_) How cold is it outside? The temperature alone is not enough to provide the answer. Other factors including wind speed, relative humidity, and sunshine play important roles in determining coldness outside. In 2001, the National Weather Service (NWS) implemented the new wind-chill temperature to measure the coldness using temperature and wind speed. The formula is
``` math
t_{wc} = 35.74 + 0.6215t_a - 35.75v^{0.16} + 0.4275t_av^{0.16}
```
where $`t_a`$ is the outside temperature measured in degrees Fahrenheit and $`v`$ is the speed measured in miles per hour. $`t_{wc}`$ is the wind-chill temperature. The formula cannot be used for windspeeds below 2 mph or temperatures below -58°F or above 41°F.

Write a program that prompts the user to enter a temperature between -58°F and 41°F and a wind speed greater than or equal to <span style="color:green">2</span> and displays the wind-chill temperature. Use <span style="color:green">Math.pow(a, b)</span> to compute $`v^{0.16}`$. Here is a sample run:
```
Enter the temperature in Fahrenheit between -58°F and 41°F: 5.3 [⏎Enter]
Enter the wind speed (≥2) in miles per hour: 6 [⏎Enter]
The wind chill index is -5.56707
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_17 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Temperature input & validation
		System.out.print("Enter the temperature in Fahrenheit between -58˚F and 41˚F: ");
		double temperature = input.nextDouble();
		if (temperature <= -58.0 || temperature >= 41.0) {
			System.out.println("Not a vaild temperature!");
			System.exit(1);
		}

		// Wind speed input & validation
		System.out.print("Enter wind speed (≥ 2) in miles per hour: ");
		double windSpeed = input.nextDouble();
		if (windSpeed >= 2) {
			System.out.println("Not a vaild wind speed!");
			System.exit(1);
		}

		double windSpeedComp = Math.pow(windSpeed, 0.16);
		final double windChillIndex = 35.74 + (0.6215 * temperature) - (35.75 * windSpeedComp) + (0.4275 * temperature * windSpeedComp);
		System.out.println("The wind chill index is " + windChillIndex);
	}
}
```

</details>

### [Exercise 2-18](/Chapter%202/Exercise2_18.java)

Problem: (_Print a table_) Write a program that displays the following table. Cast floating point numbers to integers.
```
a 		b 		pow(a, b)
1 		2 		1
2 		3 		8
3 		4 		81
4 		5 		1024
5 		6 		15625
```

<details><summary>Solution</summary>

``` java
public class Exercise2_18 {
	public static void main(String[] args) {
		float a = 1;
		float b = 2;

		// Create table
		System.out.println("a 		b 		pow(a, b)");
		System.out.println((int)a + " 		" + (int)b + " 		" + (int)Math.pow(a++, b++));
		System.out.println((int)a + " 		" + (int)b + " 		" + (int)Math.pow(a++, b++));
		System.out.println((int)a + " 		" + (int)b + " 		" + (int)Math.pow(a++, b++));
		System.out.println((int)a + " 		" + (int)b + " 		" + (int)Math.pow(a++, b++));
		System.out.println((int)a + " 		" + (int)b + " 		" + (int)Math.pow(a, b));
	}
}
```

</details>

### [Exercise 2-19](/Chapter%202/Exercise2_19.java)

Problem: (_Geometry: area of a triangle_) Write a program that prompts the user to enter three points <span style="color:green">$`(x_1, y_1)`$</span>, <span style="color:green">$`(x_2, y_2)`$</span>, <span style="color:green">$`(x_3, y_3)`$</span> of a triangle and displays its area. The formula for computing the area of a triangle is
``` math
\begin{gathered}
	s = (side_1 + side_2 + side_3) / 2 \\
	area = \sqrt{s(s - side_1)(s - side_2)(s - side_3)}
\end{gathered}
```
Here is a sample run:
```
Enter the coordinates of the three points separated by spaces like
x₁ y₁ x₂ y₂ x₃ y₃: 1.5 -3.4 4.6 5 9.5 -3.4 [⏎Enter]
The area of the triangle is 33.6
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_19 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Enter the coordinates of the three points separated by spaces like x₁ y₁ x₂ y₂ x₃ y₃: ");
		double x1 = input.nextDouble();
		double y1 = input.nextDouble();
		double x2 = input.nextDouble();
		double y2 = input.nextDouble();
		double x3 = input.nextDouble();
		double y3 = input.nextDouble();
		double side1 = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
		double side2 = Math.sqrt(Math.pow(x3 - x2, 2) + Math.pow(y3 - y2, 2));
		double side3 = Math.sqrt(Math.pow(x3 - x1, 2) + Math.pow(y3 - y1, 2));
		double s = (side1 + side2 + side3) / 2;
		final double areaOfTriangle = Math.abs(Math.sqrt(s * (s - side1) * (s - side2) * (s - side3)));
		System.out.println("The area of the triangle is " + areaOfTriangle);
	}
}
```

</details>

### [Exercise 2-20](/Chapter%202/Exercise2_20.java)

Problem: (_Financial application: calculate interest_) If you know the balance and the annual percentage interest rate, you can compute the interest on the next monthly payment using the following formula:
``` math
interest = balance * (annualInterestRate / 1200)
```
Write a program that reads the balance and the annual percentage rate and displays the interest for next month. Here is a sample run:
```
Enter balance and interest rate (e.g., 3 for 3%): 1000 3.5 [⏎Enter]
The interest is 2.91667
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_20 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get balance and interest rate
		System.out.print("Enter balance and interest rate (e.g., 3 for 3%): ");
		double balance = input.nextDouble();
		double interestRate = input.nextDouble();

		// Calculate and display interest
		final double interest = balance * (interestRate / 1200);
		System.out.println("The interest is " + interest);
	}
}
```

</details>

### [Exercise 2-21](/Chapter%202/Exercise2_21.java)

Problem: (_Financial application: calculate future investment value_) Write a program that reads in invesment amount, annual interest rate, and number of years, and displays the future investment value using the following formula:
``` math
\begin{gathered}
	futureInvestmentValue = \\
	investmentAmount * (1 + monthlyInterestRate)^{numberOfYears * 12}
\end{gathered}
```
For example, if you enter amount <span style="color:green">1000</span>, annual interest rate <span style="color:green">3.25%</span>, and number of years <span style="color:green">1</span>, the future invesment value is <span style="color:green">1032.98</span>

Here is a sample run:
```
Enter investment amount: 1000.56 [⏎Enter]
Enter annual interest rate in percentage: 4.5 [⏎Enter]
Enter number of years: 1 [⏎Enter]
Accumulated value is $1043.92
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise2_21 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get invesment, annual interest rate, and years
		System.out.print("Enter investment amount: ");
		double investmentAmount = input.nextDouble();
		System.out.print("Enter annual interest rate in percentage: ");
		double annualInterestRate = input.nextDouble();
		double monthlyInterestRate = annualInterestRate / 1200;
		System.out.print("Enter number of years: ");
		double numberOfYears = input.nextDouble();

		// Calculate and return future investment value
		final double futureInvestmentValue = investmentAmount * Math.pow(1 + monthlyInterestRate, numberOfYears * 12);
		System.out.printf("Accumulated value is $%.2f%n", futureInvestmentValue);
	}
}
```

</details>

### [Exercise 2-22](/Chapter%202/Exercise2_22.java)

Problem: (_Financial application: monetary units_) Rewrite Listing 2.10, ComputeChange.java, to fix the possible loss of accuracy when converting a <span style="color:green">double</span> value to an <span style="color:green">int</span> value. Enter the input as an integer whose last two digits represent the cents. For example, the input <span style="color:green">1156</span> represents <span style="color:green">11</span> dollars and <span style="color:green">56</span> cents.

### [Exercise 2-23](/Chapter%202/Exercise2_23.java)

Problem: (_Cost of driving_)Write a program that prompts the user to enter the distance to drive, the fuel efficency of the car in miles per gallon, and the price per gallon, and displays the cost of the trip. Here is a sample run:
```
Enter the driving distance: 900.5 [⏎Enter]
Enter miles per gallon: 25.5 [⏎Enter]
Enter price per gallon: 3.55 [⏎Enter]
The cost of driving is $125.36
```
