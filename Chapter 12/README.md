# Introduction to Java Programming, AP<sup>®</sup> Edition

## Chapter 12

### [Exercise 12-2](/Chapter%2012/Exercise12_2.java)

Problem: (_<span style="color:green">InputMismatchExeception</span>_) Write a program that prompts the user to read two integers and displays their sum. Your program should prompt the user to read the number again if the input is incorrect.

<details><summary>Solution</summary>

``` java
import java.util.Scanner;
import java.util.InputMismatchException;

public class Exercise12_2 {
	private static Scanner input = new Scanner(System.in);

	public static int inputInt(String p) {
		while (true) {
			System.out.print(p);
			try {
				return input.nextInt();
			} catch (InputMismatchException ex) {
				System.out.println("The value “" + input.next() +"” is not an integer");
				input.nextLine();
			}
		}
	}

	public static void main(String[] args) {
		// Prompt for input and validate value
		int sum = 0;
		for (int i = 0; i < 2; i++) {
			sum += inputInt("Enter an integer: ");
		}
		input.close();
		System.out.println("The sum is " + sum);
	}
}
```

</details>
