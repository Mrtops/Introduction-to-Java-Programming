/**********************************************************
* Exercise12_2.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;
import java.util.InputMismatchException;

public class Exercise12_2 {
	private static Scanner input = new Scanner(System.in);

	public static int inputInt(String p) {
		while (true) {
			System.out.print(p);
			try {
				return input.nextInt();
			} catch (InputMismatchException ex) {
				System.out.println("The value “" + input.next() +"” is not an integer");
				input.nextLine();
			}
		}
	}

	public static void main(String[] args) {
		// Prompt for input and validate value
		int sum = 0;
		for (int i = 0; i < 2; i++) {
			sum += inputInt("Enter an integer: ");
		}
		input.close();
		System.out.println("The sum is " + sum);
	}
}
