/**********************************************************
* Exercise10_10.java                                      *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise10_10 {
	public static void main(String[] args) {
		// Create Queue object
		Queue queue = new Queue();

		// Add 20 numbers from 1 to 20 into the queue
		for (int i = 1; i <= 20; i++) {
			queue.enqueue(i);
		}

		// Prints items out from queue
		while (!queue.empty()) {
			System.out.print(queue.dequeue() + " ");
		}
		System.out.println();
  }
}

class Queue {
	private int[] elements;
	private int size;

	// Constructor for elements
	Queue() {
		elements = new int[8];
	}

	// enqueue() — add element "v" to queue
	public void enqueue(int v) {
		if (size >= elements.length) {
			int[] temp = new int[elements.length * 2];
			System.arraycopy(elements, 0, temp, 0, elements.length);
			elements = temp;
		}
		elements[size++] = v;
	}

	// dequeue() — remove first element from queue
	public int dequeue() {
		int v = elements[0];
		int[] temp = new int[elements.length];
		System.arraycopy(elements, 1, temp, 0, size);
		elements = temp;
		--size;
		return v;
	}

	// empty() — return false if empty
	public boolean empty() {
		return size == 0;
	}

	// getSize() — return size of queue
	public int getSize() {
		return size;
  }
}
