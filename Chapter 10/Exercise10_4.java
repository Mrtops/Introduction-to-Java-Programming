/**********************************************************
* Exercise10_4.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise10_4 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    // Get x₁ y₁ x₂ y₂
    System.out.print("Enter point 1 and point 2 (x₁ y₁ x₂ y₂): ");
    double x1 = input.nextDouble();
    double y1 = input.nextDouble();
    double x2 = input.nextDouble();
    double y2 = input.nextDouble();

    // Create MyPoint objects
    MyPoint point1 = new MyPoint(x1, y1);
		MyPoint point2 = new MyPoint(x2, y2);

    // Return results
    System.out.println("The distance between ("
			+ point1.getX() + ", " + point1.getY() + ") and ("
			+ point2.getX() + ", " + point2.getY() + ") is: "
      + point1.distance(point2));
  }
}

class MyPoint {
  private double x;
  private double y;

  // Constructors for x, y
  MyPoint() {
    this(0, 0);
  }

  MyPoint(double x, double y) {
    this.x = x;
    this.y = y;
  }

  // Getter methods for x, y
  double getX() {
    return x;
  }

  double getY() {
    return y;
  }

  // distance() — returns distance between two points
  public double distance(MyPoint myPoint) {
    return Math.sqrt(Math.pow(myPoint.getX() - x, 2)
      + Math.pow(myPoint.getY() - y, 2));
  }

 	public double distance(double x, double y) {
 		return distance(new MyPoint(x, y));
 	}
}
