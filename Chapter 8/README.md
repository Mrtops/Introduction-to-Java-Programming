# Introduction to Java Programming, AP<sup>®</sup> Edition

## Chapter 8

### [Exercise 8-4](/Chapter%208/Exercise8_4.java)

Problem: (_Compute the weekly hours for each employee_) Suppose the weekly hours for all employees are stored in a two-dimensional array. Each row records an employee’s seven-day work hours with seven columns. For example, the following array stores the work hours for eight employees. Write a program that displays employees and their total hours in decreasing order of the total hours.

<div style="text-align:center"><img src ="/img/Exercise8-4.png"></div>

### [Exercise 8-5](/Chapter%208/Exercise8_5.java)

Problem: (_Algebra: add two matrices_) Write a method to add two matrices. The header of the method is as follows:
``` java
public static double[][] addMatrix(double[][] a, double[][] b)
```
In order to be added, the two matrices must have the same dimensions and the same or compatible types of elements. Let <span style="color:green">c</span> be the resulting matrix. Each element <span style="color:green">c<sub>ij</sub></span> is <span style="color:green">a<sub>ij</sub></span> + <span style="color:green">b<sub>ij</sub></span>. For example, for two 3 × 3 matrices <span style="color:green">a</span> and <span style="color:green">b</span>, <span style="color:green">c</span> is
``` math
\begin{pmatrix}
   a_{11} & a_{12} & a_{13} \\
   a_{21} & a_{22} & a_{23} \\
   a_{31} & a_{32} & a_{33}
\end{pmatrix}
+
\begin{pmatrix}
   b_{11} & b_{12} & b_{13} \\
   b_{21} & b_{22} & b_{23} \\
   b_{31} & b_{32} & b_{33}
\end{pmatrix}
=
\begin{pmatrix}
   a_{11} + b_{11} & a_{12} + b_{12} & a_{13} + b_{13} \\
   a_{21} + b_{21} & a_{22} + b_{22} & a_{23} + b_{23} \\
   a_{31} + b_{31} & a_{32} + b_{32} & a_{33} + b_{33}
\end{pmatrix}
```
Write a test program that prompts the user to enter two 3 × 3 matrices and displays their sum. Here is a sample run:
```
Enter matrix1: 1 2 3 4 5 6 7 8 9 [⏎Enter]
Enter matrix2: 0 2 4 1 4.5 2.2 1.1 4.3 5.2 [⏎Enter]
The matrices are added as follows
1.0 2.0 3.0     0.0 2.0 4.0     1.0 4.0 7.0
4.0 5.0 6.0  +  1.0 4.5 2.2  =  5.0 9.5 8.2
7.0 8.0 9.0     1.1 4.3 5.2     8.1 12.3 14.2
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise8_5 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    // Initalize two 3 × 3 arrays
    double[][] matrixA = createMatrix(65);
    double[][] matrixB = createMatrix(66);
    input.close();

    // Initalize one 3 × 3 array with sum of matrixA & matrixB
    double[][] matrixC = addMatrix(matrixA, matrixB);

    // Print final sum and visual representation of it
    printMatrix(matrixA, matrixB, matrixC);
  }

  public static double[][] createMatrix(int n) {
    // Get matrix values
    System.out.print("Enter a 3 × 3 matrix for matrix" + (char) n + ": ");

    double[][] matrix = new double[3][3];
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        matrix[i][j] = input.nextDouble();
      }
    }
    return matrix;
  }

  public static double[][] addMatrix(double[][] matrixA, double[][] matrixB) {
    // Add matrixA and matrixB
    double[][] matrix = new double[matrixA.length][matrixA[0].length];
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        matrix[i][j] = matrixA[i][j] + matrixB[i][j];
      }
    }
    return matrix;
  }

  public static void printMatrix(double[][] matrixA, double[][] matrixB, double[][] matrixC) {
    // Print matrixA + matrixB = matrixC
    System.out.println("The matrices are added as follows");

    for (int i = 0; i < 3; i++) {
      printRow(matrixA, i);
      System.out.print((i == 1 ? " +  " : "    "));
      printRow(matrixB, i);
      System.out.print((i == 1 ? " =  " : "    "));
      printRow(matrixC, i);
      System.out.println();
    }
  }

  public static void printRow(double[][] matrix, int i) {
    for (int j = 0; j < matrix[i].length; j++) {
      System.out.print(matrix[i][j] + " ");
    }
  }
}
```

</details>

### [Exercise 8-17](/Chapter%208/Exercise8_17.java)

Problem: (Financial tsunami) Banks lend money to each other. In tough economic times, if a bank goes bankrupt, it may not be able to pay back the loan. A bank’s total assets are its current balance plus its loans to other banks. The diagram in Figure 8.8 shows five banks. The banks’ current balances are 25, 125, 175, 75, and 181 million dollars, respectively. The directed edge from node 1 to node 2 indicates that bank 1 lends 40 million dollars to bank 2.

<div style="text-align:center"><img src ="/img/Figure8-8.png"></div>

**<span style="color:darkgreen">Figure 8.8</span>** Banks lend money to each other.

If a bank’s total assets are under a certain limit, the bank is unsafe. The money it borrowed cannot be returned to the lender, and the lender cannot count the loan in its total assets. Consequently, the lender may also be unsafe, if its total assets are under the limit. Write a program to find all the unsafe banks. Your program reads the input as follows. It first reads two integers <span style="color:green">n</span> and <span style="color:green">limit</span>, where <span style="color:green">n</span> indicates the number of banks and <span style="color:green">limit</span> is the minimum total assets for keeping a bank safe. It then reads <span style="color:green">n</span> lines that describe the information for <span style="color:green">n</span> banks with IDs from <span style="color:green">0</span> to <span style="color:green">n - 1</span>.

The first number in the line is the bank’s balance, the second number indicates the number of banks that borrowed money from the bank, and the rest are pairs of two numbers. Each pair describes a borrower. The first number in the pair is the borrower’s ID and the second is the amount borrowed. For example, the input for the five banks in Figure 8.8 is as follows (note that the limit is 201):

5 201
25 2 1 100.5 4 320.5
125 2 2 40 3 85
175 2 0 125 3 75
75 1 0 125
181 1 2 125

The total assets of bank 3 are (75 + 125), which is under 201, so bank 3 is unsafe. After bank 3 becomes unsafe, the total assets of bank 1 fall below (125 + 40). Thus, bank 1 is also unsafe. The output of the program should be
```
Unsafe banks are 3 1
```
(_Hint_: Use a two-dimensional array <span style="color:green">borrowers</span> to represent loans. <span style="color:green">borrowers\[i]\[j]</span> indicates the loan that bank <span style="color:green">i</span> loans to bank <span style="color:green">j</span>. Once bank <span style="color:green">j</span> becomes unsafe, <span style="color:green">borrowers\[i]\[j]</span> should be set to <span style="color:green">0</span>.)

### [Exercise 8-20](/Chapter%208/Exercise8_20.java)

Problem: (_Game: connect four_) Connect four is a two-player board game in which the players alternately drop colored disks into a seven-column, six-row vertically suspended grid, as shown below.

<div style="text-align:center"><img src ="/img/Exercise8-20.png"></div>

The objective of the game is to connect four same-colored disks in a row, a column, or a diagonal before your opponent can do likewise. The program prompts two players to drop a red or yellow disk alternately. In the preceding figure, the red disk is shown in a dark color and the yellow in a light color. Whenever a disk is dropped, the program redisplays the board on the console and determines the status of the game (win, draw, or continue). Here is a sample run:
```
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
-----------------------------
Drop a red disk at column (0–6): 0 [⏎Enter]
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
| R |   |   |   |   |   |   |
-----------------------------
Drop a yellow disk at column (0–6): 3 [⏎Enter]
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
| R |   |   | Y |   |   |   |
-----------------------------

.   .   .
.   .   .
.   .   .

Drop a yellow disk at column (0–6): 6 [⏎Enter]
|   |   |   |   |   |   |   |
|   |   |   |   |   |   |   |
|   |   |   | R |   |   |   |
|   |   |   | Y | R | Y |   |
|   |   | R | Y | Y | Y | Y |
| R | Y | R | Y | R | R | R |
-----------------------------
The yellow player won
```

### [Exercise 8-28](/Chapter%208/Exercise8_28.java)

Problem: (_Strictly identical arrays_) The two-dimensional arrays <span style="color:green">m1</span> and <span style="color:green">m2</span> are strictly identical if their corresponding elements are equal. Write a method that returns true if <span style="color:green">m1</span> and <span style="color:green">m2</span> are strictly identical, using the following header:
``` java
public static boolean equals(int[][] m1, int[][] m2)
```
Write a test program that prompts the user to enter two 3 × 3 arrays of integers and displays whether the two are strictly identical. Here are the sample runs.
```
Enter list1: 51 22 25 6 1 4 24 54 6 [⏎Enter]
Enter list2: 51 22 25 6 1 4 24 54 6 [⏎Enter]
The two arrays are strictly identical
```
```
Enter list1: 51 25 22 6 1 4 24 54 6 [⏎Enter]
Enter list2: 51 22 25 6 1 4 24 54 6 [⏎Enter]
The two arrays are not strictly identical
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;
import java.util.InputMismatchException;

public class Exercise8_28 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    // Initalize two arrays
    int[][] list1 = createMatrix(1);
    int[][] list2 = createMatrix(2);

    // Determine if two lists are equal and return results
    System.out.println("The two arrays are" + (equals(list1, list2) ? " " : " not ") + "strictly identical");
  }

  public static int[][] createMatrix(int n) {
    // Get list values
    System.out.print("Enter list" + n + ": ");

    int[][] list = new int[3][3];
    for (int i = 0; i < list.length; ++i) {
      for (int j = 0; j < list[i].length; ++j) {
        list[i][j] = inputInt(i, j);
      }
    }
    return list;
  }

  public static int inputInt(int i, int j) {
		while (true) {
			try {
				return input.nextInt();
			} catch (InputMismatchException ex) {
				System.out.println("The value “" + input.next() +"” at index " + i + ", " + j + " is not an integer");
        System.out.print("Please contiune input from index " + i + ", " + j + ": ");
				input.nextLine();
			}
		}
	}

  public static boolean equals(int[][] list1, int[][] list2) {
    for (int i = 0; i < list1.length; ++i) {
      for (int j = 0; j < list1[i].length; ++j) {
        if (list1[i][j] != list2[i][j]) {
          return false;
        }
      }
    }
    return true;
  }
}

```

</details>

### [Exercise 8-29](/Chapter%208/Exercise8_29.java)

Problem: (_Identical arrays_) The two-dimensional arrays <span style="color:green">m1</span> and <span style="color:green">m2</span> are identical if they have the same contents. Write a method that returns true if <span style="color:green">m1</span> and <span style="color:green">m2</span> are identical, using the following header:
``` java
public static boolean equals(int[][] m1, int[][] m2)
```
Write a test program that prompts the user to enter two 3 × 3 arrays of integers and displays whether the two are identical. Here are the sample runs.
```
Enter list1: 51 25 22 6 1 4 24 54 6 [⏎Enter]
Enter list2: 51 22 25 6 1 4 24 54 6 [⏎Enter]
The two arrays are identical
```
```
Enter list1: 51 5 22 6 1 4 24 54 6 [⏎Enter]
Enter list2: 51 22 25 6 1 4 24 54 6 [⏎Enter]
The two arrays are not identical
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;
import java.util.Arrays;
import java.util.InputMismatchException;

public class Exercise8_29 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    // Initalize two arrays
    int[][] list1 = createMatrix(1);
    int[][] list2 = createMatrix(2);

    // Determine if two lists are equal and return results
    System.out.println("The two arrays are" + (equals(list1, list2) ? " " : " not ") + "identical");
  }

  public static int[][] createMatrix(int n) {
    // Get list values
    System.out.print("Enter list" + n + ": ");

    int[][] list = new int[3][3];
    for (int i = 0; i < list.length; ++i) {
      for (int j = 0; j < list[i].length; ++j) {
        list[i][j] = inputInt(i, j);
      }
    }
    return list;
  }

  public static int inputInt(int i, int j) {
		while (true) {
			try {
				return input.nextInt();
			} catch (InputMismatchException ex) {
				System.out.println("The value “" + input.next() +"” at index " + i + ", " + j + " is not an integer");
        System.out.print("Please contiune input from index " + i + ", " + j + ": ");
				input.nextLine();
			}
		}
	}

	public static boolean equals(int[][] list1, int[][] list2) {
		int[] array1 = sort(list1);
		int[] array2 = sort(list2);
		for (int i = 0; i < list1.length; ++i) {
			if (array1[i] != array2[i])
				return false;
		}
		return true;
	}

	public static int[] sort(int[][] list) {
		int [] array = matrixToArray(list);

    // Sort rows from least to greatest
    Arrays.sort(array, 0, 3);
    Arrays.sort(array, 3, 6);
    Arrays.sort(array, 6, 9);
		return array;
	}

	public static int[] matrixToArray(int[][] list) {
		int[] array = new int[list.length * list[0].length];
		int k = 0;
		for (int i = 0; i < list.length; ++i) {
			for (int j = 0; j < list[i].length; ++j) {
				array[k++] = list[i][j];
			}
		}
		return array;
	}
}
```

</details>
