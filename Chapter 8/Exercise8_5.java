/**********************************************************
* Exercise8_5.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise8_5 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    // Initalize two 3 × 3 arrays
    double[][] matrixA = createMatrix(65);
    double[][] matrixB = createMatrix(66);
    input.close();

    // Initalize one 3 × 3 array with sum of matrixA & matrixB
    double[][] matrixC = addMatrix(matrixA, matrixB);

    // Print final sum and visual representation of it
    printMatrix(matrixA, matrixB, matrixC);
  }

  public static double[][] createMatrix(int n) {
    // Get matrix values
    System.out.print("Enter a 3 × 3 matrix for matrix" + (char) n + ": ");

    double[][] matrix = new double[3][3];
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        matrix[i][j] = input.nextDouble();
      }
    }
    return matrix;
  }

  public static double[][] addMatrix(double[][] matrixA, double[][] matrixB) {
    // Add matrixA and matrixB
    double[][] matrix = new double[matrixA.length][matrixA[0].length];
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        matrix[i][j] = matrixA[i][j] + matrixB[i][j];
      }
    }
    return matrix;
  }

  public static void printMatrix(double[][] matrixA, double[][] matrixB, double[][] matrixC) {
    // Print matrixA + matrixB = matrixC
    System.out.println("The matrices are added as follows");

    for (int i = 0; i < 3; i++) {
      printRow(matrixA, i);
      System.out.print((i == 1 ? " +  " : "    "));
      printRow(matrixB, i);
      System.out.print((i == 1 ? " =  " : "    "));
      printRow(matrixC, i);
      System.out.println();
    }
  }

  public static void printRow(double[][] matrix, int i) {
    for (int j = 0; j < matrix[i].length; j++) {
      System.out.print(matrix[i][j] + " ");
    }
  }
}
