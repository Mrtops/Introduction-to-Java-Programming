/**********************************************************
* Exercise8_28.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;
import java.util.InputMismatchException;

public class Exercise8_28 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    // Initalize two arrays
    int[][] list1 = createMatrix(1);
    int[][] list2 = createMatrix(2);

    // Determine if two lists are equal and return results
    System.out.println("The two arrays are" + (equals(list1, list2) ? " " : " not ") + "strictly identical");
  }

  public static int[][] createMatrix(int n) {
    // Get list values
    System.out.print("Enter list" + n + ": ");

    int[][] list = new int[3][3];
    for (int i = 0; i < list.length; ++i) {
      for (int j = 0; j < list[i].length; ++j) {
        list[i][j] = inputInt(i, j);
      }
    }
    return list;
  }

  public static int inputInt(int i, int j) {
		while (true) {
			try {
				return input.nextInt();
			} catch (InputMismatchException ex) {
				System.out.println("The value “" + input.next() +"” at index " + i + ", " + j + " is not an integer");
        System.out.print("Please contiune input from index " + i + ", " + j + ": ");
				input.nextLine();
			}
		}
	}

  public static boolean equals(int[][] list1, int[][] list2) {
    for (int i = 0; i < list1.length; ++i) {
      for (int j = 0; j < list1[i].length; ++j) {
        if (list1[i][j] != list2[i][j]) {
          return false;
        }
      }
    }
    return true;
  }
}
