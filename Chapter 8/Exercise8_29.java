/**********************************************************
* Exercise8_29.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;
import java.util.Arrays;
import java.util.InputMismatchException;

public class Exercise8_29 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    // Initalize two arrays
    int[][] list1 = createMatrix(1);
    int[][] list2 = createMatrix(2);

    // Determine if two lists are equal and return results
    System.out.println("The two arrays are" + (equals(list1, list2) ? " " : " not ") + "identical");
  }

  public static int[][] createMatrix(int n) {
    // Get list values
    System.out.print("Enter list" + n + ": ");

    int[][] list = new int[3][3];
    for (int i = 0; i < list.length; ++i) {
      for (int j = 0; j < list[i].length; ++j) {
        list[i][j] = inputInt(i, j);
      }
    }
    return list;
  }

  public static int inputInt(int i, int j) {
		while (true) {
			try {
				return input.nextInt();
			} catch (InputMismatchException ex) {
				System.out.println("The value “" + input.next() +"” at index " + i + ", " + j + " is not an integer");
        System.out.print("Please contiune input from index " + i + ", " + j + ": ");
				input.nextLine();
			}
		}
	}

	public static boolean equals(int[][] list1, int[][] list2) {
		int[] array1 = sort(list1);
		int[] array2 = sort(list2);
		for (int i = 0; i < list1.length; ++i) {
			if (array1[i] != array2[i])
				return false;
		}
		return true;
	}

	public static int[] sort(int[][] list) {
		int [] array = matrixToArray(list);

    // Sort rows from least to greatest
    Arrays.sort(array);
		return array;
	}

	public static int[] matrixToArray(int[][] list) {
		int[] array = new int[list.length * list[0].length];
		int k = 0;
		for (int i = 0; i < list.length; ++i) {
			for (int j = 0; j < list[i].length; ++j) {
				array[k++] = list[i][j];
			}
		}
		return array;
	}
}
