/**********************************************************
* Exercise4_2.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise4_2 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get (x₁, y₁) (x₂, y₂)
		System.out.print("Enter point 1 (latitude and longitude) in degrees: ");
		double x1 = Math.toRadians(input.nextDouble());
		double y1 = Math.toRadians(input.nextDouble());
		System.out.print("Enter point 2 (latitude and longitude) in degrees: ");
		double x2 = Math.toRadians(input.nextDouble());
		double y2 = Math.toRadians(input.nextDouble());

		// Compute great circle distance
		final double greatCircleDistance = 6371.01
				* Math.acos(Math.sin(x1) * Math.sin(x2) + Math.cos(x1) * Math.cos(x2) * Math.cos(y1 - y2));

		// Return great circle distance
		System.out.println("The distance between the two points is " + greatCircleDistance + " km");
	}
}
