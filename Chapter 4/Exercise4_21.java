/**********************************************************
* Exercise4_21.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;
import java.util.regex.Pattern;

public class Exercise4_21 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Prompt the user to enter a Social Security number
		System.out.print("Enter a SSN: ");
		String ssn = input.nextLine();

		// Check whether the input is valid
		boolean isValid = Pattern.matches("\\d{3}-\\d{2}-\\d{4}", ssn);

		// Display result
		System.out.println(ssn + " is " + ((isValid) ? "a valid " : "an invalid ") + "social security number");
	}
}
