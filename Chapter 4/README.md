# Introduction to Java Programming, AP<sup>®</sup> Edition

## Chapter 4

### [Exercise 4-2](/Chapter%204/Exercise4_2.java)

Problem: (_Geometery: great circle distance_) The great circle distance is the distance between two points on the surface of a sphere. Let $`(x_1, y_1)`$ and $`(x_2, y_2)`$ be the geographical latitude and longitude of the two points. The great circle distance between the two points can be computed using the following formula:
``` math
d = radius * \arccos(\sin(x_1) * \sin(x_2) + \cos(x_1) * \cos(x_2) * \cos(y_1 - y_2))
```
Write a program that prompts the user to enter the latitude and longitude of two points on earth in degrees and displays its great circle distance. The average earth radius is 6,371.01 km. Note that you need to convert the degrees into radians using the <span style="color:green">Math.toRadians</span> method since the Java trigonometric methods use radians. The latitude and longitude degrees in the formula are for north and west. Use negative to indicate south and east degrees. Here is a sample run:
```
Enter point 1 (latitude and longitude) in degrees: 39.55 -116.25 [⏎Enter]
Enter point 2 (latitude and longitude) in degrees: 41.5 87.37 [⏎Enter]
The distance between the two points is 10691.79183231593 km
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise4_2 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get (x₁, y₁) (x₂, y₂)
		System.out.print("Enter point 1 (latitude and longitude) in degrees: ");
		double x1 = Math.toRadians(input.nextDouble());
		double y1 = Math.toRadians(input.nextDouble());
		System.out.print("Enter point 2 (latitude and longitude) in degrees: ");
		double x2 = Math.toRadians(input.nextDouble());
		double y2 = Math.toRadians(input.nextDouble());

		// Compute great circle distance
		final double greatCircleDistance = 6371.01
				* Math.acos(Math.sin(x1) * Math.sin(x2) + Math.cos(x1) * Math.cos(x2) * Math.cos(y1 - y2));

		// Return great circle distance
		System.out.println("The distance between the two points is " + greatCircleDistance + " km");
	}
}
```

</details>

### [Exercise 4-21](/Chapter%204/Exercise4_21.java)

Problem: (_Check SSN_) Write a program that prompts the user to enter a Social Security number in the format DDD-DD-DDDD, where D is a digit. Your program should check whether the input is valid. Here are sample runs:
```
Enter a SSN: 232-23-5435 [⏎Enter]
232-23-5435 is a valid social security number
```
```
Enter a SSN: 23-23-5435 [⏎Enter]
23-23-5435 is an invalid social security number
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;
import java.util.regex.Pattern;

public class Exercise4_21 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Prompt the user to enter a Social Security number
		System.out.print("Enter a SSN: ");
		String ssn = input.nextLine();

		// Check whether the input is valid
		boolean isValid = Pattern.matches("\\d{3}-\\d{2}-\\d{4}", ssn);

		// Display result
		System.out.println(ssn + " is " + ((isValid) ? "a valid " : "an invalid ") + "social security number");
	}
}
```

</details>
