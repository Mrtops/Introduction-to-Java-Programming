/**********************************************************
* Exercise11_4.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.ArrayList;
import java.util.Scanner;

public class Exercise11_4 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    ArrayList<Integer> list = new ArrayList<>();

    // Prompt for input and validate
    System.out.print("Enter integers (input ends with 0): ");
    int value;

    do {
      value = input.nextInt();

      if (value != 0) {
        list.add(value);
      }
    } while (value != 0);

    // Display the maximum number
    System.out.println("The maximum number is " + max(list));
  }

  public static Integer max(ArrayList<Integer> list) {
    if (list == null || list.size() == 0) {
      return null;
    }

    int result = list.get(0);
    for (int i = 1; i < list.size(); i++) {
      if (result < list.get(i)) {
        result = list.get(i);
      }
    }
    return result;
  }
}
