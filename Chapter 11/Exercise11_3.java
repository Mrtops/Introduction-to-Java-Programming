/**********************************************************
* Exercise11_3.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Calendar;

public class Exercise11_3 {
  public static void main(String[] args) {
		// Create Account, SavingsAccount and CheckingAccount objects
		Account account = new Account(1122, 20000);
		SavingsAccount savings = new SavingsAccount(1001, 20000);
		CheckingAccount checking = new CheckingAccount(1004, 20000, -20);

   	// Set an annual interest rate of 4.5%
		account.setAnnualInterestRate(4.5);
		savings.setAnnualInterestRate(4.5);
		checking.setAnnualInterestRate(4.5);

		// Withdraw $2,500
		account.withdraw(2500);
		savings.withdraw(2500);
		checking.withdraw(2500);

		// Deposit $3,000
		account.deposit(3000);
		savings.deposit(3000);
		checking.deposit(3000);

		// Invoke toString methods
		System.out.println(account.toString());
		System.out.println(savings.toString());
		System.out.println(checking.toString());
  }
}

class Account {
	private int id;
	private double balance;
	private double annualInterestRate;
	private String dateCreated;
  private Calendar cal = Calendar.getInstance();

	// Constructors for Account
	Account() {
		this(0, 0);
	}

	Account(int id, double balance) {
		this.id = id;
		this.balance = balance;
		annualInterestRate = 0;
		dateCreated = String.valueOf(cal.getTime());
	}

	// Setter methods for id, balance, annualInterestRate
	void setId(int id) {
		this.id = id;
	}

	void setBalance(double balance) {
		this.balance = balance;
	}

	void setAnnualInterestRate(double annualInterestRate) {
		this.annualInterestRate = annualInterestRate;
	}

	// Getter methods for id, balance, annualInterestRate, dateCreated
	int getId() {
		return id;
	}

  double getBalance() {
		return balance;
	}

  double getAnnualInterestRate() {
		return annualInterestRate;
	}

  String getDateCreated() {
		return dateCreated.toString();
	}

  double getMonthlyInterestRate() {
		return annualInterestRate / 12;
	}

  	// getMonthlyInterest() — Returns amount of interest due each month
	public double getMonthlyInterest() {
		return balance * (getMonthlyInterestRate() / 100);
	}

	// withdraw() — Removes an amount from balance
	public void withdraw(double amount) {
		balance -= amount;
	}

	// deposit() — Adds an amount to balance
	public void deposit(double amount) {
		balance += amount;
	}

	// Return a String decription of Account class
	public String toString() {
		return "Account ID: " + getId() + "; Date created: " + getDateCreated()
			+ "; Balance: $" + String.format("%.2f", getBalance()) +
			"; Monthly interest: $" + String.format("%.2f", getMonthlyInterest());
	}
}

class CheckingAccount extends Account {
	private double overdraftLimit;

	// Constructors for CheckingAccout
	CheckingAccount() {
		super();
		overdraftLimit = -20;
	}

	CheckingAccount(int id, double balance, double overdraftLimit) {
		super(id, balance);
		this.overdraftLimit = overdraftLimit;
	}

	// Setter method for overdraftLimit
	void setOverdraftLimit(double overdraftLimit) {
		this.overdraftLimit = overdraftLimit;
	}

	// Getter method for overdraftLimit
	double getOverdraftLimit() {
		return overdraftLimit;
	}

	// withdraw() — Removes an amount from balance with checks for overdraft
	public void withdraw(double amount) {
		if (getBalance() - amount > overdraftLimit) {
			setBalance(getBalance() - amount);
		} else {
			System.out.println("Error! Amount exceeds overdraft limit.");
    }
	}

	// Return a String decription of the extended CheckingAccount class
	public String toString() {
		return super.toString() + "; Overdraft limit: $" +
		String.format("%.2f", getOverdraftLimit());
	}
}

class SavingsAccount extends Account {
	// Constructors for SavingsAccount
	SavingsAccount() {
		super();
	}

	SavingsAccount(int id, double balance) {
		super(id, balance);
	}

	// withdraw() — Removes an amount from balance with limits for overdraft
	public void withdraw(double amount) {
		if (amount < getBalance()) {
			setBalance(getBalance() - amount);
		} else {
			System.out.println("Error! Savings account overdrawn transtaction rejected");
    }
	}

  // Return a String decription of the extended SavingsAccount class
  public String toString() {
		return super.toString();
  }
}
