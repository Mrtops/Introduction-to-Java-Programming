/**********************************************************
* Exercise11_2.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Calendar;

public class Exercise11_2 {
  public static void main(String[] args) {
		// Create Person, Student, Employee, Faculty, and Staff objects
		Person person = new Person("Smith, James", "123 Main St., Norwalk, VA",
			"(800) 234-5551", "james.smith@example.com");

		Student student = new Student("Wallace, Sally", "456 William Rd., Roanoke, VA",
      "(800) 456-4567","sally.wallace@example.com", Student.SOPHOMORE);

		Employee employee = new Employee("Harlowe, William", "222 Minor Bvld., Discord, WA",
      "(800) 111-5555", "william.harlowe@example.com", 6, 24000);

		Faculty faculty = new Faculty("Henderson, Harry", "333 Major Bvld., Accord, PA",
      "(800) 432-5432", "harry.henderson@example.com", 5, 50000, "9:00am to 5:00pm", "Rank 1a");

		Staff staff = new Staff("Johnson, Henry", "123 Billikid St., Tombstone AZ",
      "(800) 555-1212", "henry.johnson@example.com", 4, 65000, "Senior Navigator");

		// Invoke toString() of Person, Student, Employee, Faculty and Staff objects
		System.out.println(person.toString());
		System.out.println(student.toString());
		System.out.println(employee.toString());
		System.out.println(faculty.toString());
		System.out.println(staff.toString());
}
}

class Person {
  private String name;
  private String address;
  private String phoneNumber;
  private String email;

  // Constructors for name, address, phoneNumber, email
  Person() {
    this("Unknown","Unknown","Unknown","Unknown");
  }

  Person(String name, String address, String phoneNumber, String email) {
    this.name = name;
    this.address = address;
    this.phoneNumber = phoneNumber;
    this.email = email;
  }

  // Setter methods for name, address, phoneNumber, email
  void setName(String name) {
    this.name = name;
  }

  void setAddress(String address) {
    this.address = address;
  }

  void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  void setEmail(String email) {
    this.email = email;
  }

  // Getter methods for name, address, phoneNumber, email
  String getName() {
    return name;
  }

  String getAddress() {
    return address;
  }

  String getPhoneNumber() {
    return phoneNumber;
  }

  String getEmail() {
    return email;
  }

  // toString() — display class name.
  public String toString() {
    return "Person: " + getName() + "; " + getAddress()
      + "; " + getPhoneNumber() + "; " + getEmail();
  }
}

class Student extends Person {
  private int status;
	public final static int FRESHMAN = 1;
	public final static int SOPHOMORE = 3;
	public final static int JUNIOR = 2;
	public final static int SENIOR = 4;

  // Extended constructor with status
	Student(String name, String address,
		String phone, String email, int status) {
		super(name, address, phone, email);
		this.status = status;
	}

	// Setter method for status
	void setStatus(int status) {
		this.status = status;
	}

	// Getter method for status
  String getStatus() {
		switch (status) {
			case 1 : return "freshman";
			case 2 : return "sophomore";
			case 3 : return "junior";
			case 4 : return "senior";
			default : return "Unknown";
		}
	}

	// Override extended toString() with new data
  @Override
	public String toString() {
		return "Student: " + getName() + "; " + getAddress()
      + "; " + getPhoneNumber() + "; " + getEmail()
      + "; " + getStatus();
  }
}

class Employee extends Person {
  private int office;
	private double salary;
  private String dateHired;
	private Calendar cal = Calendar.getInstance();

	// Extended constructor with office, salary, dateHired
	Employee(String name, String address, String phone, String email,
    int office, double salary) {
		super(name, address, phone, email);
		this.office = office;
		this.salary = salary;
		dateHired = String.valueOf(cal.getTime());
	}

  // Setter methods for office, salary, dateHired
	void setOffice(int office) {
		this.office = office;
	}

	void setSalary(double salary) {
		this.salary = salary;
	}

	void setDateHired() {
		dateHired = String.valueOf(cal.getTime());
	}

	// Getter methods for office, salary, dateHired
  int getOffice() {
		return office;
	}

	String getSalary() {
		return String.format("%.2f", salary);
	}

	String getDateHired() {
		return dateHired;
	}

	// Override extended toString() with new data
  @Override
	public String toString() {
		return "Employee: " + getName() + "; " + getAddress()
      + "; " + getPhoneNumber() + "; " + getEmail()
      + "; " + getOffice() + "; $" + getSalary()
      + "; " + getDateHired();
  }
}

class Faculty extends Employee {
	private String officeHours;
	private String rank;

  // Extended constructor with officeHours, rank
	public Faculty(String name, String address, String phone, String email,
		int office, double salary, String officeHours, String rank) {
		super(name, address, phone, email, office, salary);
		this.officeHours = officeHours;
		this.rank = rank;
	}

  // Setter methods for officeHours, rank
	void setOfficeHours(String officeHours) {
		this.officeHours = officeHours;
	}

	void setRank(String rank) {
		this.rank = rank;
	}

  // Getter methods for officeHours, rank
  String getOfficeHours() {
		return officeHours;
	}

	String getRank() {
		return rank;
	}

	// Override extended toString() with new data
  @Override
	public String toString() {
		return "Faculty: " + getName() + "; " + getAddress()
      + "; " + getPhoneNumber() + "; " + getEmail()
      + "; " + getOffice() + "; $" + getSalary()
      + "; " + getDateHired() + "; " + getOfficeHours()
      + "; " + getRank();
  }
}

class Staff extends Employee {
  private String title;

	// Extended constructor with title
	public Staff(String name, String address, String phone,
		String email, int office, double salary, String title) {
		super(name, address, phone, email, office, salary);
		this.title = title;
	}

	// Setter method for title
  void setTitle(String title) {
		this.title = title;
	}

	// Getter method for title
	String getTitle() {
		return title;
	}

	// Override extended toString() with new data
  @Override
	public String toString() {
		return "Staff: " + getName() + "; " + getAddress()
      + "; " + getPhoneNumber() + "; " + getEmail()
      + "; " + getOffice() + "; $" + getSalary()
      + "; " + getDateHired() + "; " + getTitle();
  }
}
