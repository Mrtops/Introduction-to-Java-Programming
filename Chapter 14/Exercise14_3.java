/**********************************************************
* Exercise14_3.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise14_3 {
	private static Scanner input = new Scanner(System.in);

	public static int gcd(int m, int n) {
		if (m % n == 0) {
			return n;
		} else {
			return gcd(n, m % n);
		}
	}

	public static void main(String[] args) {
		// Get two integers for input
		System.out.print("Enter the first number: ");
		int m = input.nextInt();
		System.out.print("Enter the second number: ");
		int n = input.nextInt();
		input.close();

		// Calculate and return GCD
		System.out.println("The GCD is " + gcd(m, n));
	}
}
