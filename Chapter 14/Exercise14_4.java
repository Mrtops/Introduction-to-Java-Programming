/**********************************************************
* Exercise14_4.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

public class Exercise14_4 {
  public static double m(int i) {
    if (i == 1) {
      return 1;
    } else {
      return m(i - 1) + 1.0 / i;
    }
  }

  public static void main(String[] args) {
    // Loop through 1-10 for i
    for (int i = 1; i <= 10; i++) {
      System.out.println(m(i));
    }
  }
}
