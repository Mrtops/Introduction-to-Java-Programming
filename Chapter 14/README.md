# Introduction to Java Programming, AP<sup>®</sup> Edition

## Chapter 14

### [Exercise 14-3](/Chapter%2014/Exercise14_3.java)

Problem: (_Compute greatest common divisor using recursion_) The <span style="color:green">gcd(m, n)</span> can also be defined recursively as follows:

*   If <span style="color:green">m % n</span> is <span style="color:green">0</span>, <span style="color:green">gcd(m, n)</span> is <span style="color:green">n</span>
*   Otherwise, <span style="color:green">gcd(m, n)</span> is <span style="color:green">gcd(n, m % n)</span>

Write a recursive method to find the GCD. Write a test program that prompts the user to enter two integers and displays their GCD.

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise14_3 {
	public static int gcd(int m, int n) {
		if (m % n == 0) {
			return n;
		} else {
			return gcd(n, m % n);
		}
	}
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// Get two integers for input
		System.out.print("Enter the first number: ");
		int m = input.nextInt();
		System.out.print("Enter the second number: ");
		int n = input.nextInt();
		input.close();

		// Calculate and return GCD
		System.out.println("The GCD is " + gcd(m, n));
	}
}
```

</details>

### [Exercise 14-4](/Chapter%2014/Exercise14_4.java)

Problem: (_Sum series_) Write a recursive method to compute the following series:
```math
m(i) = 1 + \frac{1}{2} + \frac{1}{3} + \dots + \frac{1}{i}
```
Write a test program that displays <span style="color:green">m(i)</span> for <span style="color:green">i</span> = <span style="color:green">1</span>, <span style="color:green">2</span>, $`\dots`$, <span style="color:green">10</span>.

<details><summary>Solution</summary>

``` java
public class Exercise14_4 {
  public static double m(int i) {
    if (i == 1) {
      return 1;
    } else {
      return m(i - 1) + 1.0 / i;
    }
  }

  public static void main(String[] args) {
		// Loop through 1-10 for i
    for (int i = 1; i <= 10; i++) {
      System.out.println(m(i));
    }
  }
}
```

</details>
