/**********************************************************
* Exercise13_7.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Calendar;

public class Exercise13_7 {
	public static void main(String[] args) {
		// Create an array of five GeometricObjects
		GeometricObject[] squares = {new Square(4.5), new Square(14),
			new Square(8.2), new Square(12), new Square(10)};

		// Display the area and invoke the howToColor
		// method for each GeometricObject
		for (int i = 0; i < squares.length; ++i) {
		 	System.out.println("\nSquare #" + (i + 1));
		 	System.out.println("Area: " + squares[i].getArea());
		 	System.out.println("How to color: " + ((Square)squares[i]).howToColor());
		 }
	}
}

interface Colorable {
	// howToColor descriptor
	String howToColor();
}

abstract class GeometricObject {
	private String color = "while";
	private boolean filled;
	private String dateCreated;
  private Calendar cal = Calendar.getInstance();

	// Constructors for GeometricObject
	GeometricObject() {
		dateCreated = String.valueOf(cal.getTime());
	}

	GeometricObject(String color, boolean filled) {
		dateCreated = String.valueOf(cal.getTime());
		this.color = color;
		this.filled = filled;
	}

	// Setter methods for color, filled
	void setColor(String color) {
		this.color = color;
	}

	void setFilled(boolean filled) {
		this.filled = filled;
	}

	// Getter methods for color, filled
	String getColor() {
		return color;
	}

  // Since filled is boolean, the get method is named isFilled
  boolean isFilled() {
  	return filled;
  }

	// Abstract getter methods for area, perimeter
	abstract double getArea();

	abstract double getPerimeter();

  // Overriden toString method
	@Override
	public String toString() {
		return "created on " + dateCreated + "\ncolor: " + color +
			" and filled: " + filled;
	}
}

class Square extends GeometricObject implements Colorable {
	private double side;

  // Constructors for side
	Square() {
	}

	Square(double side) {
		this.side = side;
	}

	Square(double side, String color, boolean filled) {
		super(color, filled);
		setSide(side);
	}

  // Getter methods for side, area, perimeter
	public double getSide() {
		return side;
	}

  // Override superclass
	@Override
	double getArea() {
		return Math.pow(side, 2);
	}

  // Override superclass
	@Override
	double getPerimeter() {
		return side * 4;
	}

  // Setter method for side
	void setSide(double side) {
		this.side = side;
	}

  // Overriden howToColor interface
	@Override
	public String howToColor() {
		return "Color all four sides";
	}

  // Overriden toString method
	@Override
	public String toString() {
		return super.toString() + "\nSide: " + side + "\nArea: " + getArea()
			+ "\nPerimeter: " + getPerimeter();
	}
}
