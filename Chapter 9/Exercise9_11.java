/**********************************************************
* Exercise9_11.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise9_11 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    // Get a, b, c, d, e, and f
		System.out.print("Enter a, b, c, d, e, f: ");
		double a = input.nextDouble();
		double b = input.nextDouble();
		double c = input.nextDouble();
		double d = input.nextDouble();
		double e = input.nextDouble();
		double f = input.nextDouble();

    // Create a LinearEquation object
		LinearEquation linearEquation = new LinearEquation(a, b, c, d, e, f);

		// Return results
		if (linearEquation.isSolvable()) {
			System.out.println("x is " + linearEquation.getX() + " and y is "
        + linearEquation.getY());
		}
		else
			System.out.println("The equation has no solution.");
  }
}

class LinearEquation {
  private double a;
  private double b;
  private double c;
  private double d;
  private double e;
  private double f;

  // Constructor for a, b, c, d, e, f
  LinearEquation(double a, double b, double c, double d, double e, double f) {
    this.a = a;
    this.b = b;
    this.c = c;
    this.d = d;
    this.e = e;
    this.f = f;
  }

  // Getter methods for a, b, c, d, e, f
  double getA() {
    return a;
  }

  double getB() {
    return b;
  }

  double getC() {
    return c;
  }

  double getD() {
    return d;
  }

  double getE() {
    return e;
  }

  double getF() {
    return f;
  }

  // isSolvable() — returns true if ad - bc is not 0
  boolean isSolvable() {
    return a * d - b * c != 0;
  }

  // getX() — returns x if equation is solvable
  double getX() {
    if (isSolvable()) {
      return (e * d - b * f) / (a * d - b * c);
    } else {
      return Double.NaN;
    }
  }

  // getY() — returns y if equation is solvable
  double getY() {
    if (isSolvable()) {
      return (a * f - e * c) / (a * d - b * c);
    } else {
      return Double.NaN;
    }
  }
}
