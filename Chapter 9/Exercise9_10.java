/**********************************************************
* Exercise9_10.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise9_10 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    System.out.print("Enter a, b, c: ");
    double a = input.nextDouble();
    double b = input.nextDouble();
    double c = input.nextDouble();
    input.close();

    QuadraticEquation quadraticEquation = new QuadraticEquation(a, b, c);
    double discriminant = quadraticEquation.getDiscriminant();

    if (discriminant < 0) {
      System.out.println("The equation has no real roots");
    }
    else if (discriminant == 0)
    {
      System.out.println("The root is " + quadraticEquation.getRoot1());
    }
    else // if (discriminant >= 0)
    {
      System.out.println("The roots are " + quadraticEquation.getRoot1() + " and " + quadraticEquation.getRoot2());
    }
  }
}

class QuadraticEquation {
  private double a;
  private double b;
  private double c;

  // Constructor for a, b, c
  QuadraticEquation(double a, double b, double c) {
    this.a = a;
    this.b = b;
    this.c = c;
  }

  // Getter methods for a, b, c
  double getA() {
    return a;
  }

  double getB() {
    return b;
  }

  double getC() {
    return c;
  }

  double getDiscriminant() {
    return Math.pow(b, 2) - 4 * a * c;
  }

  double getRoot1() {
    if (getDiscriminant() < 0)
      return 0;
    else {
      return (-b + Math.sqrt(getDiscriminant())) / (2 * a);
    }
  }

  double getRoot2() {
    if (getDiscriminant() < 0)
      return 0;
    else {
      return (-b - Math.sqrt(getDiscriminant())) / (2 * a);
    }
  }
}
