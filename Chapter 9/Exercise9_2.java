/**********************************************************
* Exercise9_2.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

public class Exercise9_2 {
  public static void main(String[] args) {
    Stock stock = new Stock("ORCL", "Oracle Corporation", 34.5, 34.35);

    // Display stock info
    System.out.println("Previous Closing Price: " +
      stock.getPreviousClosingPrice());
    System.out.println("Current Price: " +
      stock.getCurrentPrice());
    System.out.println("Price Change: " +
      stock.getChangePercent() * 100 + "%");
  }
}

class Stock {
  String symbol;
  String name;
  double previousClosingPrice;
  double currentPrice;

  Stock(String symbol, String name, double previousClosingPrice, double currentPrice) {
    this.symbol = symbol;
    this.name = name;
    this.previousClosingPrice = previousClosingPrice;
    this.currentPrice = currentPrice;
  }

  double getChangePercent() {
    return (currentPrice - previousClosingPrice) / previousClosingPrice;
  }

  double getPreviousClosingPrice() {
    return previousClosingPrice;
  }

  double getCurrentPrice() {
    return currentPrice;
  }
}
