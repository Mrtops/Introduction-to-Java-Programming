# Introduction to Java Programming, AP<sup>®</sup> Edition

## Chapter 9

### [Exercise 9-2](/Chapter%209/Exercise9_2.java)

Problem: (_The <span style="color:green">Stock</span> class_) Following the example of the <span style="color:green">Circle</span> class in Section 9.2, design a class named <span style="color:green">Stock</span> that contains:

-   A string data field named <span style="color:green">symbol</span> for the stock’s symbol.
-   A string data field named <span style="color:green">name</span> for the stock’s name.
-   A <span style="color:green">double</span> data field named <span style="color:green">previousClosingPrice</span> that stores the stock price for the previous day.
-   A <span style="color:green">double</span> data field named <span style="color:green">currentPrice</span> that stores the stock price for the current time.
-   A constructor that creates a stock with the specified symbol and name.
-   A method named <span style="color:green">getChangePercent()</span> that returns the percentage changed from <span style="color:green">previousClosingPrice</span> to <span style="color:green">currentPrice</span>.

Draw the UML diagram for the class and then implement the class. Write a test program that creates a <span style="color:green">Stock</span> object with the stock symbol <span style="color:green">ORCL</span>, the name <span style="color:green">Oracle Corporation</span>,and the previous closing price of <span style="color:green">34.5</span>. Set a new current price to <span style="color:green">34.35</span> and display the price-change percentage.

<details><summary>Solution</summary>

<div style="text-align:center"><img src ="/img/Exercise9-2UMLDiagram.png"></div>

``` java
public class Exercise9_2 {
  public static void main(String[] args) {
    Stock stock = new Stock("ORCL", "Oracle Corporation");
    stock.setPreviousClosingPrice(34.5);

    // Set current price
    stock.setCurrentPrice(34.35);

    // Display stock info
    System.out.println("Previous Closing Price: " +
      stock.getPreviousClosingPrice());
    System.out.println("Current Price: " +
      stock.getCurrentPrice());
    System.out.println("Price Change: " +
      stock.getChangePercent() * 100 + "%");
  }
}

class Stock {
  String symbol;
  String name;
  double previousClosingPrice;
  double currentPrice;

  Stock() {
  }

  Stock(String newSymbol, String newName) {
    symbol = newSymbol;
    name = newName;
  }

  double getChangePercent() {
    return (currentPrice - previousClosingPrice) /
      previousClosingPrice;
  }

  double getPreviousClosingPrice() {
    return previousClosingPrice;
  }

  double getCurrentPrice() {
    return currentPrice;
  }

  void setCurrentPrice(double newCurrentPrice) {
    currentPrice = newCurrentPrice;
  }

  void setPreviousClosingPrice(double newPreviousClosingPrice) {
    previousClosingPrice = newPreviousClosingPrice;
  }
}

```

</details>
