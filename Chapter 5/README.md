# Introduction to Java Programming, AP<sup>®</sup> Edition

## Chapter 5

### [Exercise 5-1](/Chapter%205/Exercise5_1.java)

Problem: (_Count positive and negative numbers and compute the average of numbers_) Write a program that reads an unspecified number of integers, determines how many positive and negative values have been read, and computes the total and average of the input values (not counting zeros). Your program ends with the input <span style="color:green">0</span>. Display the average as a floating-point number. Here is a sample run:
```
Enter an integer, the input ends if it is 0: 1 2 -1 3 0 [⏎Enter]
The number of positives is 3
The number of negatives is 1
The total is 5.0
The average is 1.25
```
```
Enter an integer, the input ends if it is 0: 0 [⏎Enter]
No numbers are entered except 0
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise5_1 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		int positive, negative, count;
		positive = negative = count = 0;
		double total = 0;

		// Get integers
		System.out.print("Enter an integer, the input ends if it is 0: ");
		int number = input.nextInt();
		if (number == 0) {
			System.out.println("No numbers are entered except 0");
			System.exit(1);
		}
		while (number != 0) {
			if (number > 0) {
				++positive;
			} else {
				++negative;
			}
			++count;
			total += number;
			number = input.nextInt();
		}

		// Calculate and return the average
		final double average = total / count;
		System.out.println("The number of positives is " + positive);
		System.out.println("The number of negatives is " + negative);
		System.out.println("The total is " + total);
		System.out.println("The average is " + average);
	}
}
```

</details>

### [Exercise 5-45](/Chapter%205/Exercise5_45.java)

Problem: (_Statistics: compute mean and standard deviation_) In business applications, you are often asked to compute the mean and standard deviation of data. The mean is simply the average of the numbers. The standard deviation is a statistic that tells you how tightly all the various data are clustered around the mean in a set of data. For example, what is the average age of the students in a class? How close are the ages? If all the students are the same age, the deviation is 0.

Write a program that prompts the user to enter ten numbers, and displays the mean and standard deviations of these numbers using the following formula:
``` math
\mathrm{mean} = \frac{\displaystyle\sum_{i = 1}^{n}x_i}{n} = \frac{x_1 + x_2 + \dotsi + x_n}{n} \kern1em \mathrm{deviation} = \sqrt{\frac{\displaystyle\sum_{i = 1}^{n}x_i^2 - \frac{\Bigg(\displaystyle\sum_{i = 1}^{n}x_i\Bigg)^2}{n}}{n-1}}
```
Here is a sample run:
```
Enter ten numbers: 1 2 3 4.5 5.6 6 7 8 9 10 [⏎Enter]
The mean is 5.61
The standard deviation is 2.99794
```

### [Exercise 5-49](/Chapter%205/Exercise5_49.java)

Problem: (_Count vowels and consonants_) Assume letters A, E, I, O, and U as the vowels. Write a program that prompts the user to enter a string and displays the number of vowels and consonants in the string.
```
Enter a string: Programming is fun [⏎Enter]
The number of vowels is 5
The number of consonants is 11
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise5_49 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get string
		System.out.print("Enter a string: ");
		String s = input.nextLine();

		// Initalize count for vowels and consonants
		int vowels, consonants;
		vowels = consonants = 0;

		// Count the number of vowels and consonants
		for (int i = 0; i < s.length(); i++) {
			if (Character.isLetter(s.charAt(i))) {
				if (Character.toUpperCase(s.charAt(i)) == 'A' || Character.toUpperCase(s.charAt(i)) == 'E' || Character.toUpperCase(s.charAt(i)) == 'I' || Character.toUpperCase(s.charAt(i)) == 'O' || Character.toUpperCase(s.charAt(i)) == 'U') {
					++vowels;
				} else {
					++consonants;
				}
			}
		}

		// Return number of vowels and consonants
		System.out.println("The number of vowels is " + vowels);
		System.out.println("The number of consonants is " + consonants);
	}
}
```

</details>
