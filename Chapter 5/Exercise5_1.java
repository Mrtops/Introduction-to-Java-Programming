/**********************************************************
* Exercise5_1.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise5_1 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		int positive, negative, count;
		positive = negative = count = 0;
		double total = 0;

		// Get integers
		System.out.print("Enter an integer, the input ends if it is 0: ");
		int number = input.nextInt();
		if (number == 0) {
			System.out.println("No numbers are entered except 0");
			System.exit(1);
		}
		while (number != 0) {
			if (number > 0) {
				++positive;
			} else {
				++negative;
			}
			++count;
			total += number;
			number = input.nextInt();
		}

		// Calculate and return the average
		final double average = total / count;
		System.out.println("The number of positives is " + positive);
		System.out.println("The number of negatives is " + negative);
		System.out.println("The total is " + total);
		System.out.println("The average is " + average);
	}
}
