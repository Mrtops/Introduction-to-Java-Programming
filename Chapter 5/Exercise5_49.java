/**********************************************************
* Exercise5_49.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise5_49 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get string
		System.out.print("Enter a string: ");
		String s = input.nextLine();

		// Initalize count for vowels and consonants
		int vowels, consonants;
		vowels = consonants = 0;

		// Count the number of vowels and consonants
		for (int i = 0; i < s.length(); i++) {
			if (Character.isLetter(s.charAt(i))) {
				if (Character.toUpperCase(s.charAt(i)) == 'A' || Character.toUpperCase(s.charAt(i)) == 'E' || Character.toUpperCase(s.charAt(i)) == 'I' || Character.toUpperCase(s.charAt(i)) == 'O' || Character.toUpperCase(s.charAt(i)) == 'U') {
					++vowels;
				} else {
					++consonants;
				}
			}
		}

		// Return number of vowels and consonants
		System.out.println("The number of vowels is " + vowels);
		System.out.println("The number of consonants is " + consonants);
	}
}
