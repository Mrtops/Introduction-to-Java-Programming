/**********************************************************
* Exercise7_11.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise7_11 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Create array and prompt user
		double[] numbers = new double[10];
		System.out.print("Enter ten numbers: ");
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = input.nextDouble();
		}
		input.close();

		// Reurn the mean and standard deviation
		System.out.println("The mean is " + mean(numbers));
		System.out.println("The standard deviation is " + deviation(numbers));
	}

	public static double mean(double[] numbers) {
		double mean = 0;
		for (double e: numbers) {
			mean += e;
		}
		mean /= numbers.length;
		return mean;
	}

	public static double deviation(double[] numbers) {
		double deviation = 0;
		double mean = mean(numbers);
		for (double e: numbers) {
			deviation += Math.pow(e - mean, 2);
		}
		deviation = Math.sqrt(deviation / (numbers.length - 1));
		return deviation;
	}
}
