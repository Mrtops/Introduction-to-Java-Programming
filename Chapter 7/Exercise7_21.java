/**********************************************************
* Exercise7_21.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise7_21 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    // Get number of balls
    System.out.print("Enter number of balls to drop: ");
    int balls = input.nextInt();

    // Get number of pegs (slots - 1)
    System.out.print("Enter number of slots in the bean machine: ");
    int slots = input.nextInt();
    int pegs = slots - 1;
    input.close();

    // Get paths of balls and resulting slots
    String[] paths = getPaths(balls, pegs);
    int[] filledSlots = getSlots(paths, slots);

    // Print out the paths and resulting slots of balls
    print(paths, pegs);
    print(filledSlots);
  }

  public static String[] getPaths(int numOfPaths, int pegs) {
    String[] paths = new String[numOfPaths];
    for (int i = 0; i < paths.length; i++) {
      String str = "";
      for (int j = 0; j < pegs; j++){
        if ((int) (Math.random() * 2) == 0) {
          str += 'L';
        } else {
          str += 'R';
        }
      }
      paths[i] = str;
    }
    return paths;
  }

  public static int[] getSlots(String[] paths, int pegs) {
    int[] slots = new int[pegs];
    for (int i = 0; i < paths.length; i++) {
      int numberOfRs = 0;
      for (int j = 0; j < paths[i].length(); j++) {
        if (paths[i].charAt(j) == 'R') {
          numberOfRs++;
        }
      }
      slots[numberOfRs]++;
    }
    return slots;
  }

  public static void print(String[] paths, int pegs) {
    System.out.println();
    for (int i = 0; i < paths.length; i++) {
      System.out.println(paths[i]);
    }
  }

  // Overloaded print method
  public static void print(int[] slots) {
    int max = max(slots);

    while (max > 0) {
      System.out.println();
      for (int i = 0; i < slots.length; i++) {
        if (slots[i] >= max) {
          System.out.print("O");
        } else {
          System.out.print(" ");
        }
      }
      max--;
    }
    System.out.println();
  }

  // Return max of values in array
	public static int max(int[] slots) {
		int max = slots[0];

		for (int i = 1; i < slots.length; i++) {
			if (slots[i] > max)
				max = slots[i];
		}
		return max;
	}
}
