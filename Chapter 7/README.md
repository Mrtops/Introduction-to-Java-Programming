# Introduction to Java Programming, AP<sup>®</sup> Edition

## Chapter 7

### [Exercise 7-3](/Chapter%207/Exercise7_3.java)

Problem: (_Count occurrence of numbers_) Write a program that reads the integers between 1 and 100 and counts the occurrences of each. Assume the input ends with <span style="color:green">0</span>. Here is a sample run of the program:
```
Enter the integers between 1 and 100: 2 5 6 5 4 3 23 43 2 0 [⏎Enter]
2 occurs 2 times
3 occurs 1 time
4 occurs 1 time
5 occurs 2 times
6 occurs 1 time
23 occurs 1 time
43 occurs 1 time
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise7_3 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		int[] counts = new int[100];

		// Prompt the user to enter integers and count occurrence of numbers
		System.out.print("Enter the integers between 1 and 100: ");
		count(counts);
		input.close();

		// Return results
		for (int i = 0; i < counts.length; i++) {
			if (counts[i] > 0) {
				System.out.println((i + 1) + " occurs " + counts[i] + ((counts[i] > 1) ? " times" : " time"));
			}
		}
	}

	public static void count(int[] counts){
		// Count values and check them for 0
		for (int num = input.nextInt(); num != 0; num = input.nextInt()) {
			if (num >= 1 && num <= 100)	{
				counts[num - 1]++;
			}
		}
	}
}
```

</details>

### [Exercise 7-11](/Chapter%207/Exercise7_11.java)

Problem: (_Statistics: compute deviation_) [Programming Exercise 5.45](#exercise-5-45) computes the standard deviation of numbers. This exercise uses a different but equivalent formula to compute the standard deviation of <span style="color:green">n</span> numbers.
``` math
\mathrm{mean} = \frac{\displaystyle\sum_{i = 1}^{n}x_i}{n} = \frac{x_1 + x_2 + \dotsi + x_n}{n} \kern1em \mathrm{deviation} = \sqrt{\frac{\displaystyle\sum_{i = 1}^{n}(x_i -  \mathrm{mean})^2}{n-1}}
```
To compute the standard deviation with this formula, you have to store the individual numbrs using an array, so that they can be used after the mean is obtained.

Your program should contain the following methods:
``` java
/** Compute the deviation of double values */
public static double deviation(double[] x)
```
``` java
/** Compute the mean of an array of double values */
public static double mean(double[] x)
```
Write a test program that prompts the user to enter ten numbers and displays the mean and standard deviation, as shown in the following sample run:
```
Enter ten numbers: 1.9 2.5 3.7 2 1 6 3 4 5 2 [⏎Enter]
The mean is 3.11
The standard deviation is 1.55738
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise7_11 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Create array and prompt user
		double[] numbers = new double[10];
		System.out.print("Enter ten numbers: ");
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = input.nextDouble();
		}
		input.close();

		// Reurn the mean and standard deviation
		System.out.println("The mean is " + mean(numbers));
		System.out.println("The standard deviation is " + deviation(numbers));
	}

	public static double mean(double[] numbers) {
		double mean = 0;
		for (double e: numbers) {
			mean += e;
		}
		mean /= numbers.length;
		return mean;
	}

	public static double deviation(double[] numbers) {
		double deviation = 0;
		double mean = mean(numbers);
		for (double e: numbers) {
			deviation += Math.pow(e - mean, 2);
		}
		deviation = Math.sqrt(deviation / (numbers.length - 1));
		return deviation;
	}
}
```

</details>

### [Exercise 7-20](/Chapter%207/Exercise7_20.java)

Problem: (_Revise selection sort_) In Section 7.11, you used selection sort to sort an array. The selection-sort method repeatedly finds the smallest number in the current array and swaps it with the first. Rewrite this program by finding the largest number and swapping it with the last. Write a test program that reads in ten double numbers, invokes the method, and displays the sorted numbers.

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise7_20 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
		// Create array and prompt user
		double[] numbers = new double[10];
		System.out.print("Enter ten numbers: ");
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = input.nextDouble();
		}
		input.close();

    // Sort and print array
    selectionSort(numbers);
    System.out.println("The sorted values are: ");
    for (double e: numbers) {
      System.out.print(e + " ");
    }
    System.out.println();
  }

  public static void selectionSort(double[] numbers) {
    for (int i = numbers.length - 1; i >= 0; i--) {
      // Find the maximum element in unsorted array
      double currentMax = numbers[i];
      int currentMaxIndex = i;
      for (int j = i - 1; j >= 0; j--) {
        if (currentMax < numbers[j]) {
          currentMax = numbers[j];
          currentMaxIndex = j;
        }
      }

      // Swap the found maximum element with the last element
      if (currentMaxIndex != i) {
        numbers[currentMaxIndex] = numbers[i];
				numbers[i] = currentMax;
      }
    }
  }
}
```

</details>

### [Exercise 7-21](/Chapter%207/Exercise7_21.java)

Problem: (_Game: bean machine_) The bean machine, also known as a quincunx or the Galton box, is a device for statistics experiments named after English scientist Sir Francis Galton. It consists of an upright board with evenly spaced nails (or pegs) in a triangular form, as shown in Figure 7.13.

<div style="text-align:center"><img src ="/img/Figure7-13.png"></div>

**<span style="color:darkgreen">Figure 7.13</span>** Each ball takes a random path and falls into a slot.

Balls are dropped from the opening of the board. Every time a ball hits a nail, it has a 50% chance of falling to the left or to the right. The piles of balls are accumulated in the slots at the bottom of the board.

Write a program that simulates the bean machine. Your program should prompt the user to enter the number of the balls and the number of the slots in the machine. Simulate the falling of each ball by printing its path. For example, the path for the ball in Figure 7.13b is LLRRLLR and the path for the ball in Figure 7.13c is RLRRLRR. Display the final buildup of the balls in the slots in a histogram. Here is a sample run of the program:

```
Enter the number of balls to drop: 5 [⏎Enter]
Enter the number of slots in the bean machine: 8 [⏎Enter]

LRLRLRR
RRLLLRR
LLRLLRR
RRLLLLL
LRLRRLR

    O
    O
  OOO
```
(_Hint_: Create an array named <span style="color:green">slots</span>. Each element in <span style="color:green">slots</span> stores the number of balls in a slot. Each ball falls into a slot via a path. The number of Rs in a path is the position of the slot where the ball falls. For example, for the path LRLRLRR, the ball falls into <span style="color:green">slots[4]</span>, and for the path is RRLLLLL, the ball falls into <span style="color:green">slots[2]</span>.)

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise7_21 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
    // Get number of balls
    System.out.print("Enter number of balls to drop: ");
    int balls = input.nextInt();

    // Get number of pegs (slots - 1)
    System.out.print("Enter number of slots in the bean machine: ");
    int slots = input.nextInt();
    int pegs = slots - 1;
    input.close();

    // Get paths of balls and resulting slots
    String[] paths = getPaths(balls, pegs);
    int[] filledSlots = getSlots(paths, slots);

    // Print out the paths and resulting slots of balls
    print(paths, pegs);
    print(filledSlots);
  }

  public static String[] getPaths(int numOfPaths, int pegs) {
    String[] paths = new String[numOfPaths];
    for (int i = 0; i < paths.length; i++) {
      String str = "";
      for (int j = 0; j < pegs; j++){
        if ((int) (Math.random() * 2) == 0) {
          str += 'L';
        } else {
          str += 'R';
        }
      }
      paths[i] = str;
    }
    return paths;
  }

  public static int[] getSlots(String[] paths, int pegs) {
    int[] slots = new int[pegs];
    for (int i = 0; i < paths.length; i++) {
      int numberOfRs = 0;
      for (int j = 0; j < paths[i].length(); j++) {
        if (paths[i].charAt(j) == 'R') {
          numberOfRs++;
        }
      }
      slots[numberOfRs]++;
    }
    return slots;
  }

  public static void print(String[] paths, int pegs) {
    System.out.println();
    for (int i = 0; i < paths.length; i++) {
      System.out.println(paths[i]);
    }
  }

  // Overloaded print method
  public static void print(int[] slots) {
    int max = max(slots);

    while (max > 0) {
      System.out.println();
      for (int i = 0; i < slots.length; i++) {
        if (slots[i] >= max) {
          System.out.print("O");
        } else {
          System.out.print(" ");
        }
      }
      max--;
    }
    System.out.println();
  }

  // Return max of values in array
	public static int max(int[] slots) {
		int max = slots[0];

		for (int i = 1; i < slots.length; i++) {
			if (slots[i] > max)
				max = slots[i];
		}
		return max;
	}
}
```
