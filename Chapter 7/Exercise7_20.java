/**********************************************************
* Exercise7_20.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise7_20 {
  private static Scanner input = new Scanner(System.in);

  public static void main(String[] args) {
		// Create array and prompt user
		double[] numbers = new double[10];
		System.out.print("Enter ten numbers: ");
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = input.nextDouble();
		}
		input.close();

    // Sort and print array
    selectionSort(numbers);
    System.out.println("The sorted values are: ");
    for (double e: numbers) {
      System.out.print(e + " ");
    }
    System.out.println();
  }

  public static void selectionSort(double[] numbers) {
    for (int i = numbers.length - 1; i >= 0; i--) {
      // Find the maximum element in unsorted array
      double currentMax = numbers[i];
      int currentMaxIndex = i;
      for (int j = i - 1; j >= 0; j--) {
        if (currentMax < numbers[j]) {
          currentMax = numbers[j];
          currentMaxIndex = j;
        }
      }

      // Swap the found maximum element with the last element
      if (currentMaxIndex != i) {
        numbers[currentMaxIndex] = numbers[i];
				numbers[i] = currentMax;
      }
    }
  }
}
