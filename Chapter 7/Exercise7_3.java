/**********************************************************
* Exercise7_3.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise7_3 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		int[] counts = new int[100];

		// Prompt the user to enter integers and count occurrence of numbers
		System.out.print("Enter the integers between 1 and 100: ");
		count(counts);
		input.close();

		// Return results
		for (int i = 0; i < counts.length; i++) {
			if (counts[i] > 0) {
				System.out.println((i + 1) + " occurs " + counts[i] + ((counts[i] > 1) ? " times" : " time"));
			}
		}
	}

	public static void count(int[] counts) {
		// Count values and check them for 0
		for (int num = input.nextInt(); num != 0; num = input.nextInt()) {
			if (num >= 1 && num <= 100) {
				counts[num - 1]++;
			}
		}
	}
}
