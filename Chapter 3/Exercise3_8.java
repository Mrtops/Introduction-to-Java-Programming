/**********************************************************
* Exercise3_8.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise3_8 {
	private static Scanner input = new Scanner(System.in);

	public static int switchNum(int largerNum, int smallerNum, int whichNumReturn) {
		int result;
		if (whichNumReturn == 0) {
			result = smallerNum;
		} else if (whichNumReturn == 1) {
			result = largerNum;
		}
		return result;
	}

	public static void main(String[] args) {
		// Get three integers
		System.out.print("Enter three integers: ");
		int num1 = input.nextInt();
		int num2 = input.nextInt();
		int num3 = input.nextInt();

		// Sort numbers
		if (num1 > num2) {
			num1 = switchNum(num1, num2, 0);
			num2 = switchNum(num1, num2, 1);
		}
		if (num1 > num3) {
			num1 = switchNum(num1, num3, 0);
			num3 = switchNum(num1, num3, 1);
		}
		if (num2 > num3) {
			num2 = switchNum(num2, num3, 0);
			num3 = switchNum(num2, num3, 1);
		}

		// Output numbers in acending order
		System.out.println("The sorted numbers are: " + num1 + " " + num2 + " " + num3);
	}
}
