/**********************************************************
* Exercise3_2.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise3_2 {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int number1 = (int)(System.currentTimeMillis() % 10);
        int number2 = (int)(System.currentTimeMillis() / 10 % 10);
        int number3 = (int)(System.currentTimeMillis() / 100 % 10);

        System.out.print("What is " + number1 + " + "
            + number2 + " + " + number3 + "? ");
        
        int answer = input.nextInt();

        System.out.println(number1 + " + " + number2 + " + "
            + number3 + " = " + answer + " is "
            + (number1 + number2 + number3 == answer));
    }
}