# Introduction to Java Programming, AP<sup>®</sup> Edition

## Chapter 3

### [Exercise 3-1](/Chapter%203/Exercise3_1.java)

Problem: (_Algebra: solve quadratic equations_) The two roots of a quadratic equasion $`ax^2 + bx + c = 0`$ can be obtained using the following formula:
``` math
r_1 = \frac{-b + \sqrt{b^2 - 4ac}}{2a}
\kern1em \textrm{and} \kern1em
r_2 = \frac{-b - \sqrt{b^2 - 4ac}}{2a}
```
$`b^2 - 4ac`$ is called the discriminant of the quadradic equasion. If it is positive, the equasion has two real roots. If it is zero, the equasion has one root. If it is negative, the equasion has no real roots.

Write a program that prompts the user to enter values for $`a`$, $`b`$, and $`c`$ and displays the result based on the discriminant. If the discriminant is positive, display two roots. If the discriminant is <span style="color:green">0</span>, display one root. Otherwise, display "The equasion has no real roots".

Note that you can use <span style="color:green">Math.pow(x, 0.5)</span> to compute $`\sqrt{x}`$. Here are some sample runs.
```
Enter a, b, c: 1.0 3 1 [⏎Enter]
The equasion has two roots -0.381966 and -2.61803
```
```
Enter a, b, c: 1 2.0 1 [⏎Enter]
The equasion has one root -1
```
```
Enter a, b, c: 1 2 3 [⏎Enter]
The equasion has no real roots
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise3_1 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get a, b, and c
		System.out.print("Enter a, b, c: ");
		double a = input.nextDouble();
		double b = input.nextDouble();
		double c = input.nextDouble();

		// Declare discriminant, r1, and r2
		final double discriminant = Math.pow(b, 2) - 4 * a * c;
		final double r1 = (-b + Math.sqrt(discriminant)) / 2 * a;
		final double r2 = (-b - Math.sqrt(discriminant)) / 2 * a;

		// Determine correct output
		if (discriminant < 0) {
			// Check if equasion has no real roots
			System.out.println("The equasion has no real roots");
			System.exit(1);
		} else if (discriminant == 0) {
			// Check if equasion has one real root
			System.out.println("The equasion has one root " + r1);
			System.exit(1);
		} else {
			// Else output two roots
			System.out.println("The equasion has two roots " + r1 + " and " + r2);
		}
	}
}
```

</details>

### [Exercise 3-2](/Chapter%203/Exercise3_2.java)

Problem: (_Game: add three numbers_) The program in Listing 3.1, AdditionQuiz.java, generates two integers and prompts the user to enter the sum of these two integers. Revise the program to generate three single-digit integers and prompt the user to enter the sum of these three integers.

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise3_2 {
    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        int number1 = (int)(System.currentTimeMillis() % 10);
        int number2 = (int)(System.currentTimeMillis() / 10 % 10);
        int number3 = (int)(System.currentTimeMillis() / 100 % 10);

        System.out.print("What is " + number1 + " + "
            + number2 + " + " + number3 + "? ");
        
        int answer = input.nextInt();

        System.out.println(number1 + " + " + number2 + " + "
            + number3 + " = " + answer + " is "
            + (number1 + number2 + number3 == answer));
    }
}
```

</details>

### [Exercise 3-3](/Chapter%203/Exercise3_3.java)

Problem: (_Algebra: solve $`2 \times 2`$ linear equations_) A linear equasion can be solved using Cramer's rule given in [Programming Exercise 1.13](#exercise-1-13). Write a program that prompts the user to enter $`a, b, c, d, e,`$ and $`f`$ and displays the result. If $`ad - bc`$ is <span style="color:green">0</span>, report that "The equation has no solution."
```
Enter a, b, c, d, e, f: 9.0 4.0 3.0 -5.0 -6.0 -21.0 [⏎Enter]
x is -2.0 and y is 3.0
```
```
Enter a, b, c, d, e, f: 1.0 2.0 2.0 4.0 4.0 5.0 [⏎Enter]
The equation has no solution
```

### [Exercise 3-4](/Chapter%203/Exercise3_4.java)

Problem: (_Random month_) Write a program that randomly generates an integer between 1 and 12 and displays the English month name January, February, ..., December for the number 1, 2, ..., 12, accordingly.

### [Exercise 3-8](/Chapter%203/Exercise3_8.java)

Problem: (_Sort three integers_) Write a program that prompts the user to enter three integers and display the integers in a non decreacing order.

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise3_8 {
	private static Scanner input = new Scanner(System.in);

	public static int switchNum(int largerNum, int smallerNum, int whichNumReturn) {
		int result;
		if (whichNumReturn == 0) {
			result = smallerNum;
		} else if (whichNumReturn == 1) {
			result = largerNum;
		}
		return result;
	}

	public static void main(String[] args) {
		// Get three integers
		System.out.print("Enter three integers: ");
		int num1 = input.nextInt();
		int num2 = input.nextInt();
		int num3 = input.nextInt();

		// Sort numbers
		if (num1 > num2) {
			num1 = switchNum(num1, num2, 0);
			num2 = switchNum(num1, num2, 1);
		}
		if (num1 > num3) {
			num1 = switchNum(num1, num3, 0);
			num3 = switchNum(num1, num3, 1);
		}
		if (num2 > num3) {
			num2 = switchNum(num2, num3, 0);
			num3 = switchNum(num2, num3, 1);
		}

		// Output numbers in acending order
		System.out.println("The sorted numbers are: " + num1 + " " + num2 + " " + num3);
	}
}
```

</details>

### [Exercise 3-22](/Chapter%203/Exercise3_22.java)

Problem:(_Geometry: point in a circle?_) Write a program that prompts the user to enter a point (<span style="color:green">$`x`$</span>, <span style="color:green">$`y`$</span>) and checks whether the point is within the circle centered at (<span style="color:green">0</span>, <span style="color:green">0</span>) with radius <span style="color:green">10</span>. For example, (<span style="color:green">4</span>, <span style="color:green">5</span>) is inside the circle and (<span style="color:green">9</span>, <span style="color:green">9</span>) is outside the circle, as shown in Figure 3.7a.

(_Hint_: A point is in the circle if its distance to (<span style="color:green">0</span>, <span style="color:green">0</span>) is less than or equal to <span style="color:green">10</span>. The formula for computing the distance is $`\sqrt{(x_2 - x_1)^2 + (y_2 - y_1)^2}`$. Test your program to cover all cases.) Two sample runs are shown below.
```
Enter a point with two coordinates: 4 5 [⏎Enter]
Point (4.0, 5.0) is in the circle
```
```
Enter a point with two coordinates: 9 9 [⏎Enter]
Point (9.0, 9.0) is not in the circle
```

<div style="text-align:center"><img src ="/img/Figure3-7.png"></div>

**<span style="color:darkgreen">Figure 3.7</span>** (a) Points inside and outside of the circle. (b) Points inside and outside the rectangle.

### [Exercise 3-23](/Chapter%203/Exercise3_23.java)

Problem: (_Geometry: point in a rectangle?_) Write a program that prompts the user to enter a point (<span style="color:green">$`x`$</span>, <span style="color:green">$`y`$</span>) and checks whether the point is within the rectangle centered at (<span style="color:green">0</span>, <span style="color:green">0</span>) with width <span style="color:green">10</span> and height <span style="color:green">5</span>. For example, (<span style="color:green">2</span>, <span style="color:green">2</span>) is inside the rectangle and (<span style="color:green">6</span>, <span style="color:green">4</span>) is outside the rectangle, as shown in Figure 3.7b. (_Hint_: A point is in the rectangle if its horizontal distance to (<span style="color:green">0</span>, <span style="color:green">0</span>) is less than or equal to <span style="color:green">10 / 2</span> and its vertical distance to (<span style="color:green">0</span>, <span style="color:green">0</span>) is less than or equal to <span style="color:green">5.0 / 2</span>. Test your program to cover all cases.) Here are two sample runs.
```
Enter a point with two coordinates: 2 2 [⏎Enter]
Point (2.0, 2.0) is in the rectangle
```
```
Enter a point with two coordinates: 6 4 [⏎Enter]
Point (6.0, 4.0) is not in the rectangle
```

### [Exercise 3-25](/Chapter%203/Exercise3_25.java)

Problem: (_Geometry: intersecting point_) Two points on line 1 are given as $`(`$<span style="color:green">$`x_1`$</span>$`,`$<span style="color:green">$`y_1`$</span>$`)`$ and $`(`$<span style="color:green">$`x_2`$</span>$`,`$<span style="color:green">$`y_2`$</span>$`)`$ and on line as $`(`$<span style="color:green">$`x_3`$</span>$`,`$<span style="color:green">$`y_3`$</span>$`)`$ and $`(`$<span style="color:green">$`x_4`$</span>$`,`$<span style="color:green">$`y_4`$</span>$`)`$, as shown in Figure 3.8a-b.

The intersecting point of the two lines can be found by solving the following linear equation:
``` math
\begin{aligned}
	(y_1 - y_2)x - (x_1 - x_2)y = (y_1 - y_2)x_1 - (x_1 - x_2)y_1 \\
	(y_3 - y_4)x - (x_3 - x_4)y = (y_3 - y_4)x_3 - (x_3 - x_4)y_3
\end{aligned}
```
This linear equation can be solved using Cramer's rule (see [Programming Exercise 3.3](#exercise-3-3)). If the equation has no solutions, the two lines are parallel (Figure 3.8c). Write a program that prompts the user to enter four points and displays the intersecting point. Here are sample runs:

<div style="text-align:center"><img src ="/img/Figure3-8.png"></div>

**<span style="color:darkgreen">Figure 3.8</span>** Two lines intersect in (a and b) and two lines are parallel in (&#99;).

```
Enter x₁, y₁, x₂, y₂, x₃, y₃, x₄, y₄: 2 2 5 -1.0 4.0 2.0 -1.0 -2.0 [⏎Enter]
The intersecting point is at (2.88889, 1.1111)
```
```
Enter x₁, y₁, x₂, y₂, x₃, y₃, x₄, y₄: 2 2 7 6.0 4.0 2.0 -1.0 -2.0 [⏎Enter]
The two lines are parallel
```

### [Exercise 3-27](/Chapter%203/Exercise3_27.java)

Problem: (_Geometry: points in triangle?_) Suppose a right triangle is placed in a plane as shown below. The right-angle point is placed at (0, 0), and the other two points are placed at (200, 0), and (0, 100). Write a program that prompts the user to enter a point with $`x`$- and $`y`$-coordinates and determines whether the point is inside the triangle. Here are the sample runs:

<div style="text-align:center"><img src ="/img/Exercise3-27.png"></div>

```
Enter a point's x- and y-coordinates: 100.5 25.5 [⏎Enter]
The point is in the triangle
```
```
Enter a point's x- and y-coordinates: 100.5 50.5 [⏎Enter]
The point is not in the triangle
```

### [Exercise 3-28](/Chapter%203/Exercise3_28.java)

Problem: (_Geometry: two rectangles_) Write a program that prompts the user to enter the center $`x`$-, $`y`$-coordinates, width, and height of two rectangles and determines whether the second rectangle is inside the first or overlaps with the first, as shown in Figure 3.9. Test your program to cover all cases.

<div style="text-align:center"><img src ="/img/Figure3-9.png"></div>

**<span style="color:darkgreen">Figure 3.9</span>** (a) A rectangle is inside another one. (b) A rectangle overlaps another one.

Here are the sample runs:
```
Enter r1's center x-, y-coordinates, width, and height: 2.5 4 2.5 43 [⏎Enter]
Enter r2's center x-, y-coordinates, width, and height: 1.5 5 0.5 3 [⏎Enter]
r2 is inside r1
```
```
Enter r1's center x-, y-coordinates, width, and height: 1 2 3 5.5 [⏎Enter]
Enter r2's center x-, y-coordinates, width, and height: 3 4 4.5 5 [⏎Enter]
r2 overlaps r1
```
```
Enter r1's center x-, y-coordinates, width, and height: 1 2 3 3 [⏎Enter]
Enter r2's center x-, y-coordinates, width, and height: 40 45 3 2 [⏎Enter]
r2 does not overlap r1
```

### [Exercise 3-29](/Chapter%203/Exercise3_29.java)

Problem: (_Geometry: two circles_) Write a program that prompts the user to enter the center coordinates and radii of two circles and determines whether the second circle is inside the first or overlaps with the first, as shown in Figure 3.10. (Hint: circle2 is inside circle1 if the distance between the two centers ≤|<span style="color:green">r1 - r2</span>| and circle2 overlaps circle1 if the distance between the two centers ≤ <span style="color:green">r1 + r2</span>.Test your program to cover all cases.)

<div style="text-align:center"><img src ="/img/Figure3-10.png"></div>

**<span style="color:darkgreen">Figure 3.10</span>** (a) A circle is inside another circle. (b) A circle overlaps another circle.

Here are the sample runs:
```
Enter circle1's center x-, y-coordinates, and radius: 0.5 5.1 13 [⏎Enter]
Enter circle2's center x-, y-coordinates, and radius: 1 1.7 4.5 [⏎Enter]
circle2 is inside circle1
```
```
Enter circle1's center x-, y-coordinates, and radius: 3.4 5.7 5.5 [⏎Enter]
Enter circle2's center x-, y-coordinates, and radius: 6.7 3.5 3 [⏎Enter]
circle2 overlaps circle1
```
```
Enter circle1's center x-, y-coordinates, and radius: 3.4 5.5 1 [⏎Enter]
Enter circle2's center x-, y-coordinates, and radius: 5.5 7.2 1 [⏎Enter]
circle2 does not overlap circle1
```

### [Exercise 3-31](/Chapter%203/Exercise3_31.java)

Problem: (Financials: currency exchange) Write a program that prompts the user to enter the exchange rate from currency in U.S. dollars to Chinese RMB. Prompt the user to enter <span style="color:green">0</span> to convert from U.S. dollars to Chinese RMB and <span style="color:green">1</span> to convert from Chinese RMB and U.S. dollars. Prompt the user to enter the amount in U.S. dollars or Chinese RMB to convert it to Chinese RMB or U.S. dollars, respectively. Here are the sample runs:
```
Enter the exchange rate from dollars to RMB: 6.81 [⏎Enter]
Enter 0 to convert dollars to RMB and 1 vice versa: 0 [⏎Enter]
Enter the dollar amount: 100 [⏎Enter]
$100.0 is ¥681.0
```
```
Enter the exchange rate from dollars to RMB: 6.81 [⏎Enter]
Enter 0 to convert dollars to RMB and 1 vice versa: 1 [⏎Enter]
Enter the RMB amount:  10000 [⏎Enter]
¥10000.0 is $1468.43
```
```
Enter the exchange rate from dollars to RMB: 6.81 [⏎Enter]
Enter 0 to convert dollars to RMB and 1 vice versa: 5 [⏎Enter]
Incorrect input
```

### [Exercise 3-32](/Chapter%203/Exercise3_32.java)

Problem: (_Geometry: point position_) Given a directed line from point p0$`(x_0, y_0)`$ to p1$`(x_1, y_1)`$, you can use the following condition to decide wether a point p2$`(x_2, y_2)`$ is on the left of the line, on the right, or on the same line (see Figure 3.11):
``` math
(x_1 - x_0) * (y_2 - y_0) - (x_2 - x_0) * (y_1 - y_0) \begin{cases}
	>0 &\text{p2 is on the left side of the line} \\
	=0 &\text{p2 is on the same line} \\
	<0 &\text{p2 is on the right side of the line}
\end{cases}
```

<div style="text-align:center"><img src ="/img/Figure3-11.png"></div>

**<span style="color:darkgreen">Figure 3.11</span>** (a) p2 is on the left of the line. (b) p2 is on the right of the line. (&#99;) p2 is on the same line.

Write a program that prompts the user to enter the three points for p0, p1, and p2 and displays whether p2 is on the left of the line from p0 to p1, on the right, or on the same line. Here are some sample runs.
```
Enter three points for p0, p1, and p2: 4.4 2 6.5 9.5 -5 4 [⏎Enter]
(-5.0, 4.0) is on the left side of the line from (4.4, 2.0) to (6.5, 9.5)
```
```
Enter three points for p0, p1, and p2: 1 1 5 5 2 2 [⏎Enter]
(2.0, 2.0) is on the line from (1.0, 1.0) to (5.0, 5.0)
```
```
Enter three points for p0, p1, and p2: 3.4 2 6.5 9.5 5 2.5 [⏎Enter]
(5.0, 2.5) is on the right side of the line from (3.4, 2.0) to (6.5, 9.5)
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise3_32 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get p0, p1, p2
		System.out.print("Enter three points for p0, p1, and p2: ");
		double x0 = input.nextDouble();
		double y0 = input.nextDouble();
		double x1 = input.nextDouble();
		double y1 = input.nextDouble();
		double x2 = input.nextDouble();
		double y2 = input.nextDouble();

		// Calculate location of p2
		final double pointPosition = (x1 - x0) * (y2 - y0) - (x2 - x0) * (y1 - y0);

		// Return the location of p2
		System.out.println("(" + x2 + ", " + y2 + ") is on the "
				+ ((pointPosition > 0) ? "left side of the " : ((pointPosition > 0) ? "right side of the " : ""))
				+ "line from (" + x0 + ", " + y0 + ") to (" + x1 + ", " + y1 + ")");
	}
}
```

</details>

### [Exercise 3-33](/Chapter%203/Exercise3_33.java)

Problem: (_Financial: compare costs_) Suppose you shop for rice in two different packages. You would like to write a program to compare the cost. The program prompts the user to enter the weight and price of the each package and displays the one with the better price. Here is a sample run:
```
Enter weight and price for package 1: 50 24.59 [⏎Enter]
Enter weight and price for package 2: 25 11.99 [⏎Enter]
Package 2 has a better price.
```
```
Enter weight and price for package 1: 50 25 [⏎Enter]
Enter weight and price for package 2: 25 12.5 [⏎Enter]
Two packages have the same price.
```

### [Exercise 3-34](/Chapter%203/Exercise3_34.java)

Problem: (_Geometry: point on line segment_) [Programming Exercise 3.32](exercise-3-32) shows how to test whether a point is on an unbounded line. Revise [Programming Exercise 3.32](exercise-3-32) to test whether a point is on a line segment. Write a program that prompts the user to enter the three points for p0, p1, and p2 and displays whether p2 is on the line segment from p0 to p1. Here are some sample runs:
```
Enter three points for p0, p1, and p2: 1 1 2.5 2.5 1.5 1.5 [⏎Enter]
(1.5, 1.5) is on the line segment from (1.0, 1.0) to (2.5, 2.5)
```
```
Enter three points for p0, p1, and p2:  1 1 2 2 3.5 3.5 [⏎Enter]
(3.5, 3.5) is not on the line segment from (1.0, 1.0) to (2.0, 2.0)
```
