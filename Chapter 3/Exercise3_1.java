/**********************************************************
* Exercise3_1.java                                        *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise3_1 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get a, b, and c
		System.out.print("Enter a, b, c: ");
		double a = input.nextDouble();
		double b = input.nextDouble();
		double c = input.nextDouble();

		// Declare discriminant, r1, and r2
		final double discriminant = Math.pow(b, 2) - 4 * a * c;
		final double r1 = (-b + Math.sqrt(discriminant)) / 2 * a;
		final double r2 = (-b - Math.sqrt(discriminant)) / 2 * a;

		// Determine correct output
		if (discriminant < 0) {
			// Check if equasion has no real roots
			System.out.println("The equasion has no real roots");
			System.exit(1);
		} else if (discriminant == 0) {
			// Check if equasion has one real root
			System.out.println("The equasion has one root " + r1);
			System.exit(1);
		} else {
			// Else output two roots
			System.out.println("The equasion has two roots " + r1 + " and " + r2);
		}
	}
}
