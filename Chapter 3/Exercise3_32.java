/**********************************************************
* Exercise3_32.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise3_32 {
	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// Get p0, p1, p2
		System.out.print("Enter three points for p0, p1, and p2: ");
		double x0 = input.nextDouble();
		double y0 = input.nextDouble();
		double x1 = input.nextDouble();
		double y1 = input.nextDouble();
		double x2 = input.nextDouble();
		double y2 = input.nextDouble();

		// Calculate location of p2
		final double pointPosition = (x1 - x0) * (y2 - y0) - (x2 - x0) * (y1 - y0);

		// Return the location of p2
		System.out.println("(" + x2 + ", " + y2 + ") is on the "
				+ ((pointPosition > 0) ? "left side of the " : ((pointPosition > 0) ? "right side of the " : ""))
				+ "line from (" + x0 + ", " + y0 + ") to (" + x1 + ", " + y1 + ")");
	}
}
