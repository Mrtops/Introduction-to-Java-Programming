# Introduction to Java Programming, AP<sup>®</sup> Edition

## Chapter 6

### [Exercise 6-14](/Chapter%206/Exercise6_14.java)

Problem: (_Estimate π_) π can be estimated using the following summarization:
``` math
m(i) = 4\bigg(1 - \frac{1}{3} + \frac{1}{5} - \frac{1}{7} + \frac{1}{9} - \frac{1}{11} + \dots + \frac{(-1)^{i + 1}}{2i-1}\bigg)
```
write a program that returns m(i) for a given i and write a test test program that displays the following table:

| i   | m(i)   |
|:---:|:------:|
| 1   | 4.0000 |
| 101 | 3.1515 |
| 201 | 3.1466 |
| 301 | 3.1449 |
| 401 | 3.1441 |
| 501 | 3.1436 |
| 601 | 3.1433 |
| 701 | 3.1430 |
| 801 | 3.1428 |
| 901 | 3.1427 |

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise6_14 {
	public static double estimatePi(double n) {
		double pi = 0;
		for (int i = 1; i <= n; i++) {
			pi += Math.pow(-1, i + 1) / (2 * i - 1);
		}
		pi *= 4;
		return pi;
	}
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// Get number of iterations
		System.out.print("Enter the number of iterations: ");
		double iterations = input.nextDouble();
		input.close();

		// Return result
		System.out.println("π approximated in " + iterations + "steps is " + estimatePi(iterations));
	}
}
```

</details>

### [Exercise 6-22](/Chapter%206/Exercise6_22.java)

Problem: (_Math: approximate the square root_) There are several techniques for implementing the <span style="color:green">sqrt</span> method in the <span style="color:green">Math</span> class. One such technique is known as the _Babylonian method_. It approximates the square root of a number, <span style="color:green">n</span>, by repeatedly performing a calculation using the following formula:
``` math
\mathrm{nextGuess} = \frac{\mathrm{lastGuess} + \frac{n}{\mathrm{lastGuess}}}{2}
```
When <span style="color:green">nextGuess</span> and <span style="color:green">lastGuess</span> are almost identical, <span style="color:green">nextGuess</span> is the approximated square root. The initial guess can be any positive value (e.g., <span style="color:green">1</span>). This value will be the starting value for <span style="color:green">lastGuess</span>. If the difference between <span style="color:green">nextGuess</span> and <span style="color:green">lastGuess</span> is less than a very small number, such as <span style="color:green">0.0001</span>, you can claim that <span style="color:green">nextGuess</span> is the approximated square root of <span style="color:green">n</span>. If not, <span style="color:green">nextGuess</span> becomes <span style="color:green">lastGuess</span> and the approximation process continues. Implement the following method that returns the square root of <span style="color:green">n</span>.
``` java
public static double sqrt(long n)
```

<details><summary>Solution</summary>

``` java
import java.util.Scanner;

public class Exercise6_22{
	public static double sqrt(double n){
		double lastGuess = 1;
		double nextGuess = (lastGuess + n / lastGuess) / 2;
		while (nextGuess - lastGuess > 0.0001){
			lastGuess = nextGuess;
			nextGuess = (lastGuess + n / lastGuess) / 2;
		}
		return nextGuess;
	}
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);

		// Get input value
		System.out.print("Enter a number: ");
		double n = input.nextDouble();
		input.close();

		// Call sqrt and return approximate value
		System.out.println("The approximate square root of " + n + " is: " + sqrt(n));
	}
}
```

</details>
