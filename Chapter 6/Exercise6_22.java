/**********************************************************
* Exercise6_22.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise6_22{
	public static double sqrt(double n){
		double lastGuess = 1;
		double nextGuess = (lastGuess + n / lastGuess) / 2;
		while (nextGuess - lastGuess > 0.0001){
			lastGuess = nextGuess;
			nextGuess = (lastGuess + n / lastGuess) / 2;
		}
		return nextGuess;
	}
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);

		// Get input value
		System.out.print("Enter a number: ");
		double n = input.nextDouble();
		input.close();

		// Call sqrt and return approximate value
		System.out.println("The approximate square root of " + n + " is: " + sqrt(n));
	}
}
