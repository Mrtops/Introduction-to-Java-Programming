/**********************************************************
* Exercise6_14.java                                       *
* Introduction to Java Programming                        *
*                                                         *
* Copyright © 2018 - 2020 Ethan Dye. All rights reserved. *
**********************************************************/

import java.util.Scanner;

public class Exercise6_14 {
	public static double estimatePi(double n) {
		double pi = 0;
		for (int i = 1; i <= n; i++) {
			pi += Math.pow(-1, i + 1) / (2 * i - 1);
		}
		pi *= 4;
		return pi;
	}
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// Get number of iterations
		System.out.print("Enter the number of iterations: ");
		double iterations = input.nextDouble();
		input.close();

		// Return result
		System.out.println("π approximated in " + iterations + "steps is " + estimatePi(iterations));
	}
}
